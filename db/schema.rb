# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_11_09_172813) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "committee_error_items", force: :cascade do |t|
    t.bigint "committee_id"
    t.bigint "error_item_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["committee_id"], name: "index_committee_error_items_on_committee_id"
    t.index ["error_item_id"], name: "index_committee_error_items_on_error_item_id"
  end

  create_table "committee_hearings", force: :cascade do |t|
    t.bigint "committee_id"
    t.bigint "hearing_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["committee_id"], name: "index_committee_hearings_on_committee_id"
    t.index ["hearing_id"], name: "index_committee_hearings_on_hearing_id"
  end

  create_table "committees", force: :cascade do |t|
    t.string "clics_committee_id"
    t.string "clics_lm_digest"
    t.string "meeting_dates", default: [], array: true
    t.string "meeting_uids", default: [], array: true
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "session_year"
    t.string "abbreviation"
  end

  create_table "early_errors", force: :cascade do |t|
    t.string "reportable_type"
    t.bigint "reportable_id"
    t.text "description"
    t.integer "sort_error"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "err_digest"
    t.string "session_year"
    t.bigint "report_id"
    t.index ["report_id"], name: "index_early_errors_on_report_id"
    t.index ["reportable_type", "reportable_id"], name: "index_early_errors_on_reportable"
  end

  create_table "error_items", force: :cascade do |t|
    t.string "reportable_type"
    t.bigint "reportable_id"
    t.bigint "target_page_id"
    t.integer "sort_error"
    t.string "clics_key"
    t.text "clics_value"
    t.string "target_css"
    t.text "target_value"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "err_digest"
    t.bigint "report_id"
    t.string "missing_value"
    t.index ["report_id"], name: "index_error_items_on_report_id"
    t.index ["reportable_type", "reportable_id"], name: "index_error_items_on_reportable"
    t.index ["target_page_id"], name: "index_error_items_on_target_page_id"
  end

  create_table "hearing_error_items", force: :cascade do |t|
    t.bigint "hearing_id"
    t.bigint "error_item_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["error_item_id"], name: "index_hearing_error_items_on_error_item_id"
    t.index ["hearing_id"], name: "index_hearing_error_items_on_hearing_id"
  end

  create_table "hearing_item_early_error", force: :cascade do |t|
    t.bigint "hearing_item_id"
    t.bigint "early_error_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["early_error_id"], name: "index_hearing_item_early_error_on_early_error_id"
    t.index ["hearing_item_id"], name: "index_hearing_item_early_error_on_hearing_item_id"
  end

  create_table "hearing_item_error_items", force: :cascade do |t|
    t.bigint "hearing_item_id"
    t.bigint "error_item_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["error_item_id"], name: "index_hearing_item_error_items_on_error_item_id"
    t.index ["hearing_item_id"], name: "index_hearing_item_error_items_on_hearing_item_id"
  end

  create_table "hearing_items", force: :cascade do |t|
    t.bigint "hearing_id"
    t.string "hearing_item_uuid"
    t.string "bill_num"
    t.string "summary_title"
    t.string "bill_action"
    t.string "bill_summ_url"
    t.string "comm_report_url"
    t.string "vote_summaries_url"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.jsonb "votes", default: {}
    t.index ["hearing_id"], name: "index_hearing_items_on_hearing_id"
  end

  create_table "hearings", force: :cascade do |t|
    t.string "meeting_uid"
    t.string "meeting_date"
    t.string "meeting_time"
    t.string "hearing_item_uuids", default: [], array: true
    t.string "clics_m_id_digest"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "reports", force: :cascade do |t|
    t.string "type"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "target_pages", force: :cascade do |t|
    t.string "url_filepath"
    t.string "target_slug"
    t.string "parentable_type"
    t.bigint "parentable_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["parentable_type", "parentable_id"], name: "index_target_pages_on_parentable"
  end

  create_table "vote_summaries", force: :cascade do |t|
    t.string "url"
    t.string "vote_time"
    t.string "vote_motion"
    t.string "clics_vote_digest"
    t.bigint "hearing_item_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["hearing_item_id"], name: "index_vote_summaries_on_hearing_item_id"
  end

  create_table "vote_summary_error_items", force: :cascade do |t|
    t.bigint "vote_summary_id"
    t.bigint "error_item_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["error_item_id"], name: "index_vote_summary_error_items_on_error_item_id"
    t.index ["vote_summary_id"], name: "index_vote_summary_error_items_on_vote_summary_id"
  end

  add_foreign_key "committee_error_items", "committees", on_delete: :cascade
  add_foreign_key "committee_error_items", "error_items", on_delete: :cascade
  add_foreign_key "committee_hearings", "committees", on_delete: :cascade
  add_foreign_key "committee_hearings", "hearings", on_delete: :cascade
  add_foreign_key "early_errors", "reports", on_delete: :cascade
  add_foreign_key "error_items", "reports", on_delete: :cascade
  add_foreign_key "error_items", "target_pages", on_delete: :cascade
  add_foreign_key "hearing_error_items", "error_items", on_delete: :cascade
  add_foreign_key "hearing_error_items", "hearings", on_delete: :cascade
  add_foreign_key "hearing_item_early_error", "early_errors", on_delete: :cascade
  add_foreign_key "hearing_item_early_error", "hearing_items", on_delete: :cascade
  add_foreign_key "hearing_item_error_items", "error_items", on_delete: :cascade
  add_foreign_key "hearing_item_error_items", "hearing_items", on_delete: :cascade
  add_foreign_key "hearing_items", "hearings", on_delete: :cascade
  add_foreign_key "vote_summaries", "hearing_items", on_delete: :cascade
  add_foreign_key "vote_summary_error_items", "error_items", on_delete: :cascade
  add_foreign_key "vote_summary_error_items", "vote_summaries", on_delete: :cascade
end
