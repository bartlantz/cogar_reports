class AddReportToErrorItems < ActiveRecord::Migration[6.1]
  def change
    add_reference :error_items, :report, null: true, foreign_key: true
    add_reference :early_errors, :report, null: true, foreign_key: true
  end
end
