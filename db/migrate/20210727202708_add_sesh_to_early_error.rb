class AddSeshToEarlyError < ActiveRecord::Migration[6.1]
  def change
    add_column :early_errors, :session_year, :string
  end
end
