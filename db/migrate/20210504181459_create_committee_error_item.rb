# frozen_string_literal: true

class CreateCommitteeErrorItem < ActiveRecord::Migration[6.1]
  def change
    create_table :committee_error_items do |t|
      t.references :committee, foreign_key: true
      t.references :error_item, foreign_key: true
      t.timestamps
    end
  end
end
