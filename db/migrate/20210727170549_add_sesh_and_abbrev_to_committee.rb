class AddSeshAndAbbrevToCommittee < ActiveRecord::Migration[6.1]
  def change
    add_column :committees, :session_year, :string
    add_column :committees, :abbreviation, :string
  end
end
