class AddMissingValueToErrorItems < ActiveRecord::Migration[6.1]
  def change
    add_column :error_items, :missing_value, :string
  end
end
