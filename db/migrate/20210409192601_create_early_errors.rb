# frozen_string_literal: true

class CreateEarlyErrors < ActiveRecord::Migration[6.1]
  def change
    create_table :early_errors do |t|
      t.references :reportable, polymorphic: true
      t.text :description
      t.integer :sort_error
      t.timestamps
    end
  end
end
