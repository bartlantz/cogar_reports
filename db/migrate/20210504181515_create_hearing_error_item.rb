# frozen_string_literal: true

class CreateHearingErrorItem < ActiveRecord::Migration[6.1]
  def change
    create_table :hearing_error_items do |t|
      t.references :hearing, foreign_key: true
      t.references :error_item, foreign_key: true
      t.timestamps
    end
  end
end
