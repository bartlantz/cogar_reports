# frozen_string_literal: true

class CreateHearings < ActiveRecord::Migration[6.1]
  def change
    create_table :hearings do |t|
      t.string :meeting_uid
      t.string :meeting_date
      t.string :meeting_time
      t.string :hearing_item_uuids, array: true, default: []
      t.string :clics_m_id_digest
      t.timestamps
    end
  end
end
