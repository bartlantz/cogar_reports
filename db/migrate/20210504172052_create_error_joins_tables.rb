# frozen_string_literal: true

class CreateErrorJoinsTables < ActiveRecord::Migration[6.1]
  # TODO: early errors might not have a committee or bill or anything. But also
  # they might. Need to look into whether it's necessary, and how to handle.

  def change
    create_table :committee_early_error do |t|
      t.references :committee, foreign_key: true
      t.references :early_error, foreign_key: true
      t.timestamps
    end
  end

  def change
    create_table :hearing_error_item do |t|
      t.references :hearing, foreign_key: true
      t.references :early_error, foreign_key: true
      t.timestamps
    end
  end

  def change
    create_table :hearing_item_early_error do |t|
      t.references :hearing_item, foreign_key: true
      t.references :early_error, foreign_key: true
      t.timestamps
    end
  end
end
