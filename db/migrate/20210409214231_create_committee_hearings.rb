# frozen_string_literal: true

class CreateCommitteeHearings < ActiveRecord::Migration[6.1]
  def change
    create_table :committee_hearings do |t|
      t.references :committee, foreign_key: true
      t.references :hearing, foreign_key: true
      t.timestamps
    end
  end
end
