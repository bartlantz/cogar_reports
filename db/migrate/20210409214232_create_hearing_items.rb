# frozen_string_literal: true

class CreateHearingItems < ActiveRecord::Migration[6.1]
  def change
    create_table :hearing_items do |t|
      t.references :hearing, foreign_key: true
      t.string :hearing_item_uuid
      t.string :bill_num
      t.string :summary_title
      t.string :bill_action
      t.string :bill_summ_url
      t.string :comm_report_url
      t.string :vote_summaries_url
      t.timestamps
    end
  end
end
