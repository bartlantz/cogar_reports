# frozen_string_literal: true

# == Schema Information
#
# Table name: reports
#
#  id         :bigint           not null, primary key
#  type       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
module Reports
  class Report < ApplicationRecord
    has_many :error_items #, class_name: 'Errors::ErrorItem'
    has_many :early_errors #, class_name: 'Errors::EarlyErrors'
    # def error_items
    #   ErrorItem.where(reportable_id: id)
    # end
  end
end
