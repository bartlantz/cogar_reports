# frozen_string_literal: true

# == Schema Information
#
# Table name: committees
#
#  id                 :bigint           not null, primary key
#  abbreviation       :string
#  clics_lm_digest    :string
#  meeting_dates      :string           default([]), is an Array
#  meeting_uids       :string           default([]), is an Array
#  session_year       :string
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  clics_committee_id :string
#
class Committee < ApplicationRecord
  has_one :target_page
  has_many :committee_hearings
  has_many :hearings, through: :committee_hearings
  has_many :hearing_items, through: :hearings
  has_many :committee_error_items
  has_many :error_items, through: :committee_error_items

  validates :clics_lm_digest, :clics_committee_id, presence: true, uniqueness: true
  validates :session_year, :abbreviation, presence: true

  before_validation :set_sesh_and_abbrev

  def self.committees_for_filter
    committees = order(session_year: :desc, abbreviation: :asc).map do |c|
      [c.clics_committee_id, c.id]
    end
    committees.unshift(%w[All nil])
  end

  def set_sesh_and_abbrev
    return nil unless clics_committee_id

    sesh = clics_committee_id[-5..]
    abbrev = clics_committee_id[0..-6].gsub('_', '')
    self.session_year =  sesh
    self.abbreviation =  abbrev
  end
end
