# frozen_string_literal: true

# == Schema Information
#
# Table name: early_errors
#
#  id              :bigint           not null, primary key
#  description     :text
#  err_digest      :string
#  reportable_type :string
#  session_year    :string
#  sort_error      :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  report_id       :bigint
#  reportable_id   :bigint
#
# Indexes
#
#  index_early_errors_on_report_id   (report_id)
#  index_early_errors_on_reportable  (reportable_type,reportable_id)
#
# Foreign Keys
#
#  fk_rails_...  (report_id => reports.id) ON DELETE => cascade
#
class EarlyError < ApplicationRecord
  belongs_to :reportable, polymorphic: true, optional: true
  belongs_to :report, optional: true

  validates :sort_error, :description, presence: true

  before_save :set_session_year

  enum sort_error: DataErrors.enum_errors

  def set_session_year
    Sessions.all.each do |sesh|
      has_sesh = description.include?(sesh.upcase) || description.include?(sesh.downcase)
      self.session_year = sesh if has_sesh
      break if session_year.present?
    end
  end
end
# end
