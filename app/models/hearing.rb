# frozen_string_literal: true

# == Schema Information
#
# Table name: hearings
#
#  id                 :bigint           not null, primary key
#  clics_m_id_digest  :string
#  hearing_item_uuids :string           default([]), is an Array
#  meeting_date       :string
#  meeting_time       :string
#  meeting_uid        :string
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#
class Hearing < ApplicationRecord
  has_many :committee_hearings
  has_many :committees, through: :committee_hearings
  has_many :hearing_items
  has_many :hearing_error_items, through: :hearing_items
  has_many :error_items, through: :hearing_error_items

  # Note that CLICS will reuse a meeting for multiple committees even if it is
  # defined by a single committee's uid. Example: The last meeting_uid is valid
  # because it was some kind of joint meeting
  # {
  #   "committee_name": "H_PBH_2021A",
  #   "meeting_dates": [
  #   "2021-01-22T10:22:48Z",
  #   "2021-02-16T13:31:40Z",
  #   "2021-02-26T11:30:00Z"
  #   ],
  #   "meeting_uids": [
  #   "H_PBH_2021A_20210122_102248",
  #   "H_PBH_2021A_20210216_133140",
  #   "H_HI_2021A_20210226_114102"   <--- THIS ONE IS JOINT
  #   ]
  # }
  # Therefore we cannot validate uniqueness of meeting_uid or digest
  validates :clics_m_id_digest, :meeting_uid, presence: true
end
