# frozen_string_literal: true

# == Schema Information
#
# Table name: target_pages
#
#  id              :bigint           not null, primary key
#  parentable_type :string
#  target_slug     :string
#  url_filepath    :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  parentable_id   :bigint
#
# Indexes
#
#  index_target_pages_on_parentable  (parentable_type,parentable_id)
#
class TargetPage < ApplicationRecord
  belongs_to :parentable, polymorphic: true
  belongs_to :committee, class_name: 'Committee', foreign_key: 'parentable_id', optional: true
  has_many :hearing, through: :committee
  has_many :hearing_item, through: :hearing

  validates :url_filepath, presence: true
  validates :target_slug, presence: true, if: :committee_parentable

  def self.interim_or_yearround(value)
    return nil unless value.present?

    value = value.to_s
    return value.downcase if value.downcase == 'senate'
    return value.downcase if value.downcase == 'house'
    return 'interim' if value.downcase.include?('interim')
    return 'year-round' if value.downcase.include?('joint')

    'other'
  end

  def committee_parentable
    parentable_type == 'Committee'
  end

  def committee
    parentable if parentable_type == 'Committee'
  end

  def committee_errors(report)
    return nil unless committee

    # Returns an AR collection of ErrorItems asociated with a single committee
    # for a given Report
    ErrorItem
      .joins('INNER JOIN committee_error_items ON error_items.id = committee_error_items.error_item_id ')
      .where(report_id: report.id, committee_error_items: { committee_id: committee.id })
  end

  def any_hearing_errors(hearings, report)
    # Returns an AR collection of ErrorItems asociated with any hearing for
    # a given Report. Hearings (aka Meetings) can be associated with multiple
    # Committees
    hearing_ids = hearings.pluck(:id)

    ErrorItem.joins('INNER JOIN hearing_error_items ON error_items.id = hearing_error_items.error_item_id').where(
      report_id: report.id, hearing_error_items: { hearing_id: hearing_ids }
    )
  end

  def any_hearing_items_errors(hearings, report)
    # Returns an AR collection of ErrorItems asociated with any hearing_item for
    # a given Report. Hearings (aka Meetings) can be associated with multiple
    # Committees, and have many hearing items

    # hearings MUST be an AR collection...
    hearing_item_ids = hearings.includes(:hearing_items).pluck('hearing_items.id')

    ErrorItem.joins('INNER JOIN hearing_item_error_items ON error_items.id = hearing_item_error_items.error_item_id').where(
      report_id: report.id, hearing_item_error_items: { hearing_item_id: hearing_item_ids }
    )
  end

  def hearing_errors(hearing_id, report)
    # Returns an AR collection of ErrorItems asociated with a single hearing for
    # a given Report. Hearings (aka Meetings) can be associated with multiple
    # Committees

    ErrorItem.joins('INNER JOIN hearing_error_items ON error_items.id = hearing_error_items.error_item_id').where(
      report_id: report.id, hearing_error_items: { hearing_id: hearing_id }
    )
  end

  def hearing_item_errors(hearing_item_id, report)
    # Returns an AR collection of ErrorItems asociated with a single
    # hearing_item for a given Report.
    ErrorItem.joins('INNER JOIN hearing_item_error_items ON error_items.id = hearing_item_error_items.error_item_id').where(
      report_id: report.id, hearing_item_error_items: { hearing_item_id: hearing_item_id }
    )
  end
end
