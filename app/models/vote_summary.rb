# frozen_string_literal: true

# == Schema Information
#
# Table name: vote_summaries
#
#  id                :bigint           not null, primary key
#  clics_vote_digest :string
#  url               :string
#  vote_motion       :string
#  vote_time         :string
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  hearing_item_id   :bigint
#
# Indexes
#
#  index_vote_summaries_on_hearing_item_id  (hearing_item_id)
#
# Foreign Keys
#
#  fk_rails_...  (hearing_item_id => hearing_items.id)
#
class VoteSummary < ApplicationRecord
  belongs_to :hearing_item
  has_one :hearing, through: :hearing_item
  has_many :committees, through: :hearing

  has_many :vote_summary_error_items
  has_many :error_items, through: :vote_summary_error_items

  validates :hearing_item_id, :clics_vote_digest, presence: true
end
