# frozen_string_literal: true

# == Schema Information
#
# Table name: error_items
#
#  id              :bigint           not null, primary key
#  clics_key       :string
#  clics_value     :text
#  err_digest      :string
#  missing_value   :string
#  reportable_type :string
#  sort_error      :integer
#  target_css      :string
#  target_value    :text
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  report_id       :bigint
#  reportable_id   :bigint
#  target_page_id  :bigint
#
# Indexes
#
#  index_error_items_on_report_id       (report_id)
#  index_error_items_on_reportable      (reportable_type,reportable_id)
#  index_error_items_on_target_page_id  (target_page_id)
#
# Foreign Keys
#
#  fk_rails_...  (report_id => reports.id) ON DELETE => cascade
#  fk_rails_...  (target_page_id => target_pages.id) ON DELETE => cascade
#
class ErrorItem < ApplicationRecord
  self.filter_attributes = []

  # belongs_to :reportable, polymorphic: true, optional: true
  belongs_to :report, optional: true
  belongs_to :target_page

  has_one :committee_error_item
  has_one :committee, through: :committee_error_item

  has_one :hearing_error_item
  has_one :hearing, through: :hearing_error_item

  has_one :hearing_item_error_item
  has_one :hearing_item, through: :hearing_item_error_item

  has_one :vote_summary_error_item
  has_one :vote_summary, through: :vote_summary_error_item

  validates :target_page, :sort_error, presence: true

  enum sort_error: DataErrors.enum_errors
end
