# frozen_string_literal: true

# == Schema Information
#
# Table name: committee_error_items
#
#  id            :bigint           not null, primary key
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  committee_id  :bigint
#  error_item_id :bigint
#
# Indexes
#
#  index_committee_error_items_on_committee_id   (committee_id)
#  index_committee_error_items_on_error_item_id  (error_item_id)
#
# Foreign Keys
#
#  fk_rails_...  (committee_id => committees.id) ON DELETE => cascade
#  fk_rails_...  (error_item_id => error_items.id) ON DELETE => cascade
#
class CommitteeErrorItem < ApplicationRecord
  belongs_to :committee
  belongs_to :error_item
end
