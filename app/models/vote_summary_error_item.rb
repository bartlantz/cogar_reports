# frozen_string_literal: true

# == Schema Information
#
# Table name: vote_summary_error_items
#
#  id              :bigint           not null, primary key
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  error_item_id   :bigint
#  vote_summary_id :bigint
#
# Indexes
#
#  index_vote_summary_error_items_on_error_item_id    (error_item_id)
#  index_vote_summary_error_items_on_vote_summary_id  (vote_summary_id)
#
# Foreign Keys
#
#  fk_rails_...  (error_item_id => error_items.id)
#  fk_rails_...  (vote_summary_id => vote_summaries.id)
#
class VoteSummaryErrorItem < ApplicationRecord
  belongs_to :error_item
  belongs_to :vote_summary

  has_one :hearing_item, through: :vote_summary
  has_one :hearing, through: :hearing_item
  has_one :committee, through: :hearing

  def committees
    Committee.includes(:committee_hearings, :hearings, :hearing_items,
                       :vote_summaries).where(vote_summaries: { id: vote_summary_id })
  end
end
