# frozen_string_literal: true

module Sessions
  def self.all
    %w[2016a 2017a 2017b 2018a 2019a 2020a 2020b 2021a 2022a]
  end
end
