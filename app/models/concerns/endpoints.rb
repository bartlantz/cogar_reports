# frozen_string_literal: true

class Endpoints
  def clics_sitename
    'http://www2.leg.state.co.us/Public/cgaAPI.nsf/api.xsp/'
  end

  def cogar_sitename
    'http://cwr-staging.denvertech.org/'
    # 'https://cogar.denvertech.org/'
    # 'http://localhost:3001/'
  end

  def localhost
    'http://localhost:3000/'
  end

  def cogar_committees_endpoint
    'committees.json'
  end

  def cogar_cmhi_endpoint
    'committee_meeting_hearing_items.json'
  end

  def clics_info_lm_path
    'committees/info/lm/'
  end

  def clics_summary_lm_path
    'committees/summary/lm/'
  end

  def clics_summary_m_id_path
    'committees/summary/mId/'
  end

  def session_start_date_2021a
    '2021-01-05T00:00:00Z'
  end

  def session_start_date_2019a
    '2019-01-05T00:00:00Z'
  end

  def session_start_date_2015a
    '2015-01-01T00:00:00Z'
  end
end
