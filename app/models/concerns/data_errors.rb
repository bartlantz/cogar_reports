# frozen_string_literal: true

class DataErrors
  def self.enum_errors
    comparison_errors.merge(early_errors)
  end

  def self.early_errors
    {
      'system: error saving object': 1,
      'clics: json error': 2,
      'coga api: json error': 3
    }
  end

  def self.comparison_errors
    {
      ok: 0,
      'coga page: application error': 10,
      'clics api: json empty': 4,
      # 'clics and coga empty values': 5,
      'clics empty value': 6,
      'coga empty value': 7,
      'coga mismatched value': 8,
      'coga incorrect order or missing values': 9
    }
  end

  def self.early_errors_for_ui
    early_errors.collect { |key, val| [key, val] }.unshift(%w[None none], %w[All nil])
  end

  def self.comparison_errors_for_ui
    comparison_errors.collect { |key, val| [key, val] }.unshift(%w[None none])
  end
end
