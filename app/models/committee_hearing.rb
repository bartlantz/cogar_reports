# frozen_string_literal: true

# == Schema Information
#
# Table name: committee_hearings
#
#  id           :bigint           not null, primary key
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  committee_id :bigint
#  hearing_id   :bigint
#
# Indexes
#
#  index_committee_hearings_on_committee_id  (committee_id)
#  index_committee_hearings_on_hearing_id    (hearing_id)
#
# Foreign Keys
#
#  fk_rails_...  (committee_id => committees.id)
#  fk_rails_...  (hearing_id => hearings.id)
#
class CommitteeHearing < ApplicationRecord
  belongs_to :committee
  belongs_to :hearing
end
