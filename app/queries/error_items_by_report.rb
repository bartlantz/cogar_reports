# frozen_string_literal: true

# frozen_string_literal: true ErrorItemsByReport collects error_items associated
# with a report and returns an array of hashes that can be written to CSV, or
# rendered in a View.

# Hashes have the following keys:
# id (error_item.id)
# url_filepath (either the Committee TargetPage or the VoteSummary TargetPage)
# clics_committee_id
# session_year
# abbreviation
# meeting_uid
# hearing_item_uuid
# vote_summary_hi_id
# sort_error
# clics_key
# clics_value
# target_css
# target_value

class ErrorItemsByReport
  def initialize(report)
    @report = report
  end

  def call
    ordered_set = ordered_error_items
    replace_enum_with_string(ordered_set)
  end

  def ordered_error_items
    sql = "
      SELECT DISTINCT error_items.id, target_pages.url_filepath, committees.clics_committee_id,committees.session_year, committees.abbreviation, hearings.meeting_uid, hearing_items.hearing_item_uuid, vote_summaries.hearing_item_id AS vote_summary_hi_id, error_items.sort_error, error_items.clics_key, error_items.clics_value, error_items.target_css, error_items.target_value

      FROM error_items
      LEFT JOIN vote_summary_error_items ON error_items.id = vote_summary_error_items.error_item_id
      LEFT JOIN hearing_item_error_items ON error_items.id = hearing_item_error_items.error_item_id
      LEFT JOIN hearing_error_items ON error_items.id = hearing_error_items.error_item_id
      LEFT JOIN committee_error_items ON error_items.id = committee_error_items.error_item_id

      LEFT JOIN vote_summaries on vote_summary_error_items.vote_summary_id = vote_summaries.id
      OR vote_summary_error_items.vote_summary_id = vote_summaries.id
      LEFT JOIN hearing_items on vote_summaries.hearing_item_id = hearing_items.id
      OR hearing_item_error_items.hearing_item_id = hearing_items.id
      LEFT JOIN hearings on hearing_items.hearing_id = hearings.id
      OR hearing_error_items.hearing_id = hearings.id
      LEFT JOIN committee_hearings on committee_hearings.hearing_id = hearings.id
      LEFT JOIN committees on committee_hearings.committee_id = committees.id OR committee_error_items.committee_id = committees.id
      LEFT JOIN target_pages ON error_items.target_page_id = target_pages.id

      WHERE error_items.report_id = #{@report.id}

      ORDER BY committees.session_year DESC, committees.abbreviation,  hearings.meeting_uid DESC NULLS FIRST, hearing_items.hearing_item_uuid DESC NULLS FIRST, vote_summary_hi_id NULLS FIRST, error_items.id, target_pages.url_filepath DESC
      "

    ActiveRecord::Base.connection.execute(sql).to_a
  end

  def replace_enum_with_string(ordered_set)
    enum_string_by_int = ErrorItem.sort_errors.invert
    ordered_set.map do |ei|
      ei['sort_error'] = enum_string_by_int[ei['sort_error']]
      ei
    end
  end
end
