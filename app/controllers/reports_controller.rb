# frozen_string_literal: true

class ReportsController < ApplicationController
  before_action :find_report, except: :index

  def index
    @reports = Reports::Report.all.order(id: :desc).limit(10)
    @recent_report = Reports::Report.all.present? ? Reports::Report.last : nil
  end

  def show
    shared_dash_vars
    dropdown_collections
  end

  def filtered
    shared_dash_vars
    dropdown_collections

    # Save a copy of current filters to pass along to the lazy-loaded
    # views (#stats, #filtered_ee, #filtered_ei)
    @f_params = params_for_filters
  end

  def stats
    # lazy-loaded via hotwire from #show or #filtered
    @stats = ::Filters::Stats.new(@report, params[:f_params]).stats
    @error_hash = DataErrors.enum_errors.invert
  end

  def filtered_ee
    # lazy-loaded via hotwire from #filtered
    if params[:f_params] && params[:f_params][:day_diff].present?
      day_diff = params[:f_params][:day_diff]
      day_diff_errs = ::Filters::DayDiffErrors.new(day_diff, @report, 'early_errors')
      @filtered_early_errors = day_diff_errs.errs
    else
      @filtered_early_errors = ::Filters::RansackEarlyErrors.new(params[:f_params], @report.id).call
    end
    @filtered_early_errors
  end

  def filtered_ei
    # lazy-loaded via hotwire from #filtered
    if params[:f_params] && params[:f_params][:day_diff].present?
      day_diff = params[:f_params][:day_diff]
      day_diff_errs = ::Filters::DayDiffErrors.new(day_diff, @report, 'error_items')
      filtered_ei = day_diff_errs.errs
    else
      filtered_ei = ::Filters::RansackErrorItems.new(params[:f_params], @report.id).call
    end
    @filtered_error_items = ::Filters::ErrorItemsToHash.new(filtered_ei).call
  end

  private

  def find_report
    report_id = params[:report_id] || params[:id]
    @report = Reports::Report.find(report_id)
  end

  def shared_dash_vars
    @recent_report = Reports::Report.last
    @current_page = "Report #{@report.id}" if Reports::Report.last.id != @report.id
    @current_session = Sessions.all.last
    @humanized_filters = ::Filters::HumanizedFilters.humanized_filters(params)
  end

  def dropdown_collections
    @early_sort_errors = DataErrors.early_errors_for_ui
    @comparison_sort_errors = DataErrors.comparison_errors_for_ui
    @sessions = Sessions.all.reverse.collect { |s| [s, s] }.unshift(%w[All nil])
    @committee_origins = [%w[All nil], %w[House house], %w[Senate senate], %w[Conference conference], %w[Joint joint]]
    @committee_ids = Committee.committees_for_filter
  end

  def params_for_filters
    filtered_params = {}
    filtered_params['day_diff'] = params['day_diff']
    filtered_params['early_sort_errors'] = params['early_sort_errors']
    filtered_params['comparison_sort_errors'] = params['comparison_sort_errors']
    filtered_params['sessions'] = params['sessions']
    filtered_params['committee_origins'] = params['committee_origins']
    filtered_params['committee_ids'] = params['committee_ids']
    filtered_params
  end
end
