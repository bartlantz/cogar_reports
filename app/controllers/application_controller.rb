# frozen_string_literal: true

class ApplicationController < ActionController::Base
  protect_from_forgery

  before_action :reset_current_page

  def reset_current_page
    @current_page = nil
  end
end
