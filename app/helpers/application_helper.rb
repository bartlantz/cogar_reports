# frozen_string_literal: true

module ApplicationHelper
  def report_timestamp(date)
    date.strftime('%m/%d/%Y at %H:%M:%S')
  end

  def cogar_url(url)
    cogar = Endpoints.new.cogar_sitename
    "#{cogar}#{url}"
  end

  def truncated_msg(msg)
    msg = "#{msg[0..600]}  ... Message Truncated. See database for full text." if msg.length > 600
    msg.split('|')
  end

  def hearing_name(ei_hash)
    return '-' unless ei_hash['meeting_uid'].present?

    m_date = ei_hash['meeting_date'] if ei_hash['meeting_date']
    m_time = ei_hash['meeting_time'] if ei_hash['meeting_time']
    m_uid = ei_hash['meeting_uid'] if ei_hash['meeting_uid']
    "#{m_date} #{m_time} (#{m_uid})"
  end

  def hearing_item_name(ei_hash)
    return '-' unless ei_hash['hearing_item_uuid'].present?

    hi_bill_num = ei_hash['bill_num'] if ei_hash['bill_num']
    hi_uid = ei_hash['hearing_item_uuid'] if ei_hash['hearing_item_uuid']
    "#{hi_bill_num} (#{hi_uid})"
  end

  def str_or_null_for_clics_value(val)
    if val.present? && val.include?('http://www2.leg.state.co.us')
      val = "#{val[0..70].gsub('http://www2.leg.state.co.us',
                               'http://www2.leg.state.co.us ')} ...[truncated]"
    end
    val.present? ? val : 'null'
  end

  def str_or_nil(val)
    val.present? ? val : 'nil'
  end

  def sub_underscore(val)
    val.present? ? val.gsub('_', '_ ') : val
  end

  def sub_dot(val)
    val.present? ? val.gsub('.', ' .') : val
  end

  def selected_or_none(params, key)
    return 'none' unless params.present?

    params[key].present? ? params[key] : 'none'
  end

  def selected_or_nil(params, key)
    return 'nil' unless params.present?

    params[key].present? ? params[key] : 'nil'
  end

  def selected_or_current(params, key, current_sesh)
    return current_sesh unless params.present?

    params[key].present? ? params[key] : current_sesh
  end

  def selected_ids_with_comm_name(params)
    return [] unless params.present? && params['committee_ids'.present?]

    params['committee_ids'].map do |id|
      [id, Committee.find(id).clics_committee_id]
    end
  end
end
