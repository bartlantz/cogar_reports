# frozen_string_literal: true

module Filters
  class RansackCommitteeOrigins
    include Shared

    def initialize(query_all_resources, params)
      @q_all = query_all_resources
      @params = params
    end

    def call
      # There is no committee_origins field, so to hack Ransack, use the same
      # field as committee_id, but use 'i' for case insensitive.

      committee_origins = @params[:committee_origins]&.reject(&:blank?) if @params[:committee_origins]
      return @q_all if return_early(committee_origins)

      committee_origins = get_search_terms_from_params(committee_origins)

      load_committees(committee_origins)
      load_hearings(committee_origins)
      load_hearing_items(committee_origins)
      load_vote_summaries(committee_origins)

      @q_all
    end

    def get_search_terms_from_params(committee_origins)
      adapted_comm_ids = []
      adapted_comm_ids << 'H_' if committee_origins.include?('house')
      adapted_comm_ids << 'S_' if committee_origins.include?('senate')
      adapted_comm_ids << 'CC_' if committee_origins.include?('conference')
      adapted_comm_ids << %w[I_ J_ O_ SE_] if committee_origins.include?('joint')
      adapted_comm_ids.flatten
    end

    def load_committees(committee_origins)
      # erroritem.committee_error_item.committee.clics_committee_id
      c_key = %w[committee_error_item committee clics_committee_id start_any].join('_')
      @q_all[:committees][c_key.to_sym] = committee_origins
    end

    def load_hearings(committee_origins)
      # erroritem.hearing_error_item.hearing.committee_hearings.committee.clics_committee_id
      h_key = %w[hearing_error_item hearing committee_hearings committee clics_committee_id start_any].join('_')
      @q_all[:hearings][h_key.to_sym] = committee_origins
    end

    def load_hearing_items(committee_origins)
      # erroritem.hearing_item_error_item.hearing_item.hearing.committee_hearings.committee.clics_committee_id
      hi_key = %w[hearing_item_error_item hearing_item hearing committee_hearings committee clics_committee_id
                  start_any].join('_')
      @q_all[:hearing_items][hi_key.to_sym] = committee_origins
    end

    def load_vote_summaries(committee_origins)
      # erroritem.vote_summary_error_item.vote_summary.hearing_item.hearing.committee_hearings.committee.clics_committee_id
      vs_key = %w[vote_summary_error_item vote_summary hearing_item hearing committee_hearings committee clics_committee_id
                  start_any].join('_')
      @q_all[:vote_summaries][vs_key.to_sym] = committee_origins
    end
  end
end
