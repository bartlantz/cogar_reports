# frozen_string_literal: true

module Filters
  class ErrorItemsToHash
    # TURN THIS INTO A HELPER?
    # def get_vs_url(vote_error_items)
    #   # Per committee TargetPage, find the associated VoteSummary TargetPage's url
    #   return '' unless vote_error_items.present?

    #   vote_error_items.first.target_page.url_filepath
    # end

    def initialize(filtered_ei)
      # filtered_ei is a collection from Ransacking that may still need to be ordered.
      # CAN I USE THE SQL FROM THE REPORT TO ORDER THESE ERRORITEMS???
      @filtered_ei = filtered_ei
    end

    def call
      ei_ids = @filtered_ei.pluck(:id)
      return {} if ei_ids.nil?

      ordered_hashes = get_ordered_hashes(ei_ids)
      ordered_hashes = replace_enum_with_string(ordered_hashes)
      # FIXME: Lauren's massive sql in get_ordered_hashes has duplicates (broken) so need to call uniq!
      # So, um uniq! returns nil if no duplicates are found and removed.
      cc_ids = ordered_hashes.map { |h| h['clics_committee_id'] }.uniq
      return {} if cc_ids.nil?


      ei_data_by_ccid = cc_ids.map do |ccid|
        ordered_hashes.map do |ei|
          ei['clics_committee_id'] == ccid ? ei : nil
        end.compact
      end.compact
      ei_data_by_ccid
    end

    def get_ordered_hashes(ei_ids)
      return [] if ei_ids.empty?
      sql = "
        SELECT DISTINCT
        target_pages.url_filepath,
        committees.clics_committee_id, committees.session_year, committees.abbreviation,
        hearings.meeting_uid, hearings.meeting_date, hearings.meeting_time,
        hearing_items.hearing_item_uuid,hearing_items.bill_num,
        vote_summaries.hearing_item_id AS vote_summary_hi_id,
        error_items.id, error_items.sort_error, error_items.clics_key, error_items.clics_value, error_items.target_css, error_items.target_value

        FROM error_items
        LEFT JOIN vote_summary_error_items ON error_items.id = vote_summary_error_items.error_item_id
        LEFT JOIN hearing_item_error_items ON error_items.id = hearing_item_error_items.error_item_id
        LEFT JOIN hearing_error_items ON error_items.id = hearing_error_items.error_item_id
        LEFT JOIN committee_error_items ON error_items.id = committee_error_items.error_item_id

        LEFT JOIN vote_summaries on vote_summary_error_items.vote_summary_id = vote_summaries.id
        OR vote_summary_error_items.vote_summary_id = vote_summaries.id
        LEFT JOIN hearing_items on vote_summaries.hearing_item_id = hearing_items.id
        OR hearing_item_error_items.hearing_item_id = hearing_items.id
        LEFT JOIN hearings on hearing_items.hearing_id = hearings.id
        OR hearing_error_items.hearing_id = hearings.id
        LEFT JOIN committee_hearings on committee_hearings.hearing_id = hearings.id
        LEFT JOIN committees on committee_hearings.committee_id = committees.id OR committee_error_items.committee_id = committees.id
        LEFT JOIN target_pages ON error_items.target_page_id = target_pages.id

        WHERE error_items.id IN (#{ei_ids.join(',')})

        ORDER BY committees.session_year DESC, committees.abbreviation,  hearings.meeting_uid DESC NULLS FIRST, hearing_items.hearing_item_uuid DESC NULLS FIRST, vote_summary_hi_id NULLS FIRST, error_items.id, target_pages.url_filepath DESC
        "
      ActiveRecord::Base.connection.execute(sql).to_a
    end

    def replace_enum_with_string(ordered_set)
      enum_string_by_int = ErrorItem.sort_errors.invert
      ordered_set.map do |ei|
        ei['sort_error'] = enum_string_by_int[ei['sort_error']]
        ei
      end
    end
  end
end
