# frozen_string_literal: true

module Filters
  class RansackErrorItems
    # attr_reader :qc, :qh, :qhi

    def initialize(params, report_id)
      # The ransack method and its hash 'q' are from the ransack gem. The #ransack method is called off an ARel collection.
      @q_all = {
        q: {},
        committees: {},
        hearings: {},
        hearing_items: {},
        vote_summaries: {}
      }

      @params = params
      @report_id = report_id
    end

    def call
      # Note that empty string and the string " 'nil' " are params that mean
      # there were no selections or "All" was selected, so we should return all
      # values
      Rails.logger.error @params[:comparison_sort_errors]
      return [] if @params && @params[:comparison_sort_errors]&.include?('none')
      return search unless any_params

      load_q_by_resource_type

      search
    end

    def load_q_by_resource_type
      @q_all = Filters::RansackSortErrors.new(@q_all, @params).call

      comm_ids = @params[:committee_ids]&.reject(&:blank?)
      @q_all = Filters::RansackCommitteeIds.new(@q_all, @params).call if comm_ids.present?

      if !comm_ids.present? || comm_ids&.include?('nil')
        @q_all = Filters::RansackSessions.new(@q_all, @params).call
        @q_all = Filters::RansackCommitteeOrigins.new(@q_all, @params).call

      end
    end

    def search
      # Because of the exclusive searching, if we want to include error_items
      # for committees, hearing, hearing_items, & votes_summaries, we have to
      # search separately using a copy of the sort_error, then manually add them
      # to the same set.
      filtered_ei = ErrorItem.where(report_id: @report_id)
      keys = %i[committees hearings hearing_items vote_summaries]

      all_ids = keys.map do |k|
        q = @q_all[:q].to_hash.merge(@q_all[k])
        filtered_ei.ransack(q).result.pluck(:id)
      end

      ErrorItem.where(id: all_ids.flatten.uniq).order(:id)
    end

    def any_params
      # If there are any params, return true. However, if early_sort_errors is
      # set to none, the code should have already returned from this class.
      # Therefore we don't need to check that param.
      return false if !@params || @params.empty?

      if @params[:committee_ids].present? || @params[:sessions].present? || @params[:committee_origins].present?
        return true
      end

      false
    end
  end
end
