# frozen_string_literal: true

module Filters
  class RansackSessions
    include Shared

    def initialize(query_all_resources, params)
      @q_all = query_all_resources
      @params = params
    end

    def call
      sessions = @params[:sessions]&.reject(&:blank?)
      return @q_all if return_early(sessions)

      load_committees(sessions)
      load_hearings(sessions)
      load_hearing_items(sessions)
      load_vote_summaries(sessions)

      @q_all
    end

    def load_committees(sessions)
      # erroritem.committee_error_item.committee.session_year
      c_key = %w[committee_error_item committee session_year i_cont_any].join('_')
      @q_all[:committees][c_key.to_sym] = sessions
    end

    def load_hearings(sessions)
      # erroritem.hearing_error_item.hearing.committee_hearings.committee.session_year
      h_key = %w[hearing_error_item hearing committee_hearings committee session_year i_cont_any].join('_')
      @q_all[:hearings][h_key.to_sym] = sessions
    end

    def load_hearing_items(sessions)
      # erroritem.hearing_item_error_item.hearing_item.hearing.committee_hearings.committee.session_year
      hi_key = %w[hearing_item_error_item hearing_item hearing committee_hearings committee session_year
                  i_cont_any].join('_')
      @q_all[:hearing_items][hi_key.to_sym] = sessions
    end

    def load_vote_summaries(sessions)
      # erroritem.vote_summary_error_item.vote_summary.hearing_item.hearing.committee_hearings.committee.session_year
      vs_key = %w[vote_summary_error_item vote_summary_hearing_item hearing committee_hearings committee session_year
                  i_cont_any].join('_')
      @q_all[:vote_summaries][vs_key.to_sym] = sessions
    end
  end
end
