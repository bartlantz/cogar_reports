# frozen_string_literal: true

module Filters
  class DayDiffErrors
    attr_reader :errs

    def initialize(day_diff, report, err_kind)
      @errs = []
      call(day_diff, report, err_kind)
    end

    def call(day_diff, report, err_kind)
      #  Parsing to a Date object instad of DateTime will put our calculations between midnight and midnight
      report_date = Date.parse(report.created_at.to_s)
      diff_date = report_date - day_diff.to_i.days
      diff_date_report = get_report(diff_date)
      return nil unless diff_date_report.present?

      non_shared_errs = get_non_shared_errs(report, diff_date_report, err_kind)

      @errs = get_ee(report.id, non_shared_errs) if err_kind == 'early_errors'
      @errs = get_ei(report.id, non_shared_errs) if err_kind == 'error_items'
    end

    def get_report(diff_date)
      Reports::CommitteeReport
        .where('created_at < ? AND created_at > ?', diff_date + 1, diff_date)
        .order(created_at: :desc)
        .last
    end

    def get_non_shared_errs(report, diff_date_report, err_kind)
      # Using 'try!' with '&.' because a report might not have any errors
      report_errs = report.try!(err_kind)&.pluck(:err_digest)
      dd_errs = diff_date_report.try!(err_kind)&.pluck(:err_digest)

      get_difference(report_errs, dd_errs)
    end

    def get_difference(report_errs, dd_errs)
      #  Only get a set of errors that are new to the current report when
      #  compared to the other report. So if there is an error on the older
      #  report that doesn't exist on the current report, we don't keep it.

      current_report_errs = {}
      report_errs.each { |re| current_report_errs[re] = 0 }
      dd_errs.each { |de| current_report_errs.delete(de) }

      current_report_errs.keys
    end

    def get_ee(report_id, non_shared_errs)
      EarlyError
        .where(report_id: report_id)
        .where(err_digest: non_shared_errs)
        .where.not(sort_error: 0)
        .order(session_year: :desc, description: :asc)
    end

    def get_ei(report_id, non_shared_errs)
      ErrorItem
        .where(report_id: report_id)
        .where(err_digest: non_shared_errs)
        .where.not(sort_error: [0, 5])
    end
  end
end
