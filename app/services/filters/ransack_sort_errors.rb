# frozen_string_literal: true

module Filters
  class RansackSortErrors
    include Shared

    def initialize(query_all_resources, params)
      @q_all = query_all_resources
      @params = params
    end

    def call
      # error_items.sort_error; use sql's IN to check for value in array of
      # enum integers
      sort_errors = @params[:comparison_sort_errors]&.reject(&:blank?)
      return @q_all if return_early(sort_errors)

      # sort_error_in is a key in q, not its own key
      @q_all[:q][:sort_error_in] = sort_errors.map(&:to_i)

      @q_all
    end
  end
end
