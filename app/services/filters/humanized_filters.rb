# frozen_string_literal: false

module Filters
  module HumanizedFilters
    def self.humanized_filters(params)
      return day_diff_msg(params) if params[:day_diff].present?

      str = ''

      proper_nouns.each do |k, v|
        val_list = get_param_val(params, k)
        str << " #{v}: #{val_list.join(', ')} |" if val_list.present?
      end

      # If there is a string of values, remove the trailing pipe
      str.present? ? str[0..-2] : 'All Report Errors'
    end

    def self.proper_nouns
      {
        early_sort_errors: 'Early Errors',
        comparison_sort_errors: 'Comparison Errors',
        sessions: 'Sessions',
        committee_origins: 'Comm Origins',
        committee_ids: 'Committee Ids'
      }
    end

    def self.get_param_val(params, key)
      vals =  params[key]
      if vals.present?
        vals.reject! { |p| p == '' }
        return ['All'] if vals.length == 1 && vals[0] == 'nil'
        return ['None'] if vals.length == 1 && vals[0] == 'none'

        if %i[early_sort_errors comparison_sort_errors].include?(key)
          errs = DataErrors.enum_errors.invert
          vals = vals.map { |str_num| errs[str_num.to_i].to_s }
        end

        vals = vals.map { |id| Committee.find(id).clics_committee_id } if key == :committee_ids

        vals = vals.map(&:titleize) if key == :committee_origins
      end

      vals
    end

    def self.day_diff_msg(params)
      day = case params[:day_diff]
            when '1'
              'Yesterday'
            when '3'
              '3 days ago'
            when '5'
              '5 days ago'
            when '7'
              '1 week ago'
            end
      "New issues since #{day}"
    end
  end
end
