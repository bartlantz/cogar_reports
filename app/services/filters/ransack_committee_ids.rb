# frozen_string_literal: true

module Filters
  class RansackCommitteeIds
    include Shared

    def initialize(query_all_resources, params)
      @q_all = query_all_resources
      @params = params
    end

    def call
      # For ErrorItems, we can search the row id for a committee
      committee_ids = @params[:committee_ids]&.reject(&:blank?)
      return @q_all if return_early(committee_ids)

      committee_ids = committee_ids.map(&:to_i)

      load_committees(committee_ids)
      load_hearings(committee_ids)
      load_hearing_items(committee_ids)
      load_vote_summaries(committee_ids)

      @q_all
    end

    def load_committees(committee_ids)
      # erroritem.committee_error_item.committee_id
      c_key = %w[committee_error_item committee_id in].join('_')
      @q_all[:committees][c_key.to_sym] = committee_ids
    end

    def load_hearings(committee_ids)
      # erroritem.hearing_error_item.hearing.committee_hearings.committee_id
      h_key = %w[hearing_error_item hearing committee_hearings committee_id in].join('_')
      @q_all[:hearings][h_key.to_sym] = committee_ids
    end

    def load_hearing_items(committee_ids)
      # erroritem.hearing_item_error_item.hearing_item.hearing.committee_hearings.committee_id
      hi_key = %w[hearing_item_error_item hearing_item hearing committee_hearings committee_id in].join('_')
      @q_all[:hearing_items][hi_key.to_sym] = committee_ids
    end

    def load_vote_summaries(committee_ids)
      # erroritem.vote_summary_error_item.vote_summary.hearing_item.hearing.committee_hearings.committee_id
      vs_key = %w[vote_summary_error_item vote_summary hearing_item hearing committee_hearings committee_id
                  in].join('_')
      @q_all[:vote_summaries][vs_key.to_sym] = committee_ids
    end
  end
end
