# frozen_string_literal: true

module Filters
  module Shared
    def return_early(collection)
      return true if collection.nil?
      return true if collection.empty?
      return true if collection.include?('nil')
      return true if collection.include?('none')

      false
    end
  end
end
