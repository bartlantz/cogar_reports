# frozen_string_literal: true

module Filters
  class RansackEarlyErrors
    def initialize(params, report_id)
      # The ransack method and its hash 'q' are from the ransack gem. The #ransack method is called off an ARel collection.
      @q = {}
      @params = params
      @report_id = report_id
    end

    def call
      return [] if @params && @params[:early_sort_errors]&.include?('none')

      load_query_values if any_params

      search
    end

    def load_query_values
      q_sort_errors

      comm_ids = @params[:committee_ids]&.reject(&:blank?) if @params[:committee_ids]
      q_committee_ids(comm_ids) if comm_ids.present? && !comm_ids.include?('nil')

      if !comm_ids.present? || (comm_ids.present? && comm_ids.include?('nil'))
        q_sessions
        q_committee_origins
      end
    end

    def q_sort_errors
      sort_errors = @params[:early_sort_errors]&.reject(&:blank?)
      return nil if return_early(sort_errors)

      # early_errors.sort_error; use sql's IN to check for value in array of
      # enum integers
      @q[:sort_error_in] = sort_errors.map(&:to_i)
    end

    def q_committee_ids(c_ids)
      committee_ids = Committee
                      .where(id: c_ids)
                      .pluck(:clics_committee_id)

      # There is no committee for an early_error, so to hack Ransack, find the
      # committee name, and search the description for the name
      # early_errors.description, contains any in array
      @q[:description_cont_any] = committee_ids if committee_ids.present?
    end

    def q_sessions
      sessions = @params[:sessions]&.reject(&:blank?)
      return nil if return_early(sessions)

      # early_errors.session_year, case-insensitive, contains any in array
      @q[:session_year_i_cont_any] = sessions
    end

    def q_committee_origins
      committee_origins = @params[:committee_origins]&.reject(&:blank?) if @params[:committee_origins]
      return nil if return_early(committee_origins)

      adapted_comm_ids = []
      adapted_comm_ids << 'H_' if committee_origins.include?('house')
      adapted_comm_ids << 'S_' if committee_origins.include?('senate')
      adapted_comm_ids << 'CC_' if committee_origins.include?('conference')
      adapted_comm_ids << %w[I_ J_ O_ SE_] if committee_origins.include?('joint')
      committee_origins = adapted_comm_ids.flatten

      # There is no session field, so to hack Ransack, search the description.
      # Use 'i' for case-insensitive to prevent key collisions with the
      # committee search.
      # early_errors.description, case-insensitive, contains any in array
      @q[:description_cont_any] = committee_origins
    end

    def return_early(collection)
      return true if collection.nil?
      return true if collection.empty?
      return true if collection.include?('nil')
      return true if collection.include?('none')

      false
    end

    def any_params
      # If there are any params, return true. However, if early_sort_errors is
      # set to none, the code should have already returned from this class.
      # Therefore we don't need to check that param.
      return false if !@params || @params.empty?

      if @params[:committee_ids].present? || @params[:sessions].present? || @params[:committee_origins].present?
        return true
      end

      false
    end

    def search
      report = Reports::Report.find(@report_id)
      filtered_early_errors = EarlyError.where(report_id: report.id)
      result = filtered_early_errors.ransack(@q).result
      result.order(session_year: :desc, description: :asc)
    end
  end
end
