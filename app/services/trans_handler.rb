# frozen_string_literal: true

# TransHandler class handles specific Model CRUD actions to prevent a Job from crashing.
class TransHandler
  def self.trans_struct
    OpenStruct.new(clazz: '', attrs: {}, context_msg: '')
  end

  def initialize(trans_struct)
    @clazz = trans_struct.clazz
    @attrs = trans_struct.attrs
    @context = trans_struct.context_msg
  end

  def create_simple
    resource = nil
    begin
      resource = clazz.constantize.create!(attrs)
    rescue StandardError => e
      Rails.logger.error e
      resource = nil
    ensure
      # Ensure & return to make sure that the Job continues...even though
      # Rubocop doesn't like it.
      return resource
    end
    resource
  end
end
