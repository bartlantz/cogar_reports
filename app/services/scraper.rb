# frozen_string_literal: true

# require 'net/https'
# require 'open-uri'
# require 'uri'
require 'nokogiri'

# Scraper tries a url and retrieves the html as a Nokogiri object or an error
class Scraper
  def initialize
    @result = OpenStruct.new(doc: nil, error: false)
  end

  def call(report, target_url)
    begin
      @result.doc = Nokogiri::HTML(URI.parse(target_url).open)
    rescue StandardError => e
      record_error(target_url, report, e)
      @result.error = true
    end
    @result
  end

  def record_error(target_url, report, err)
    msg = "Could not reach TargetPage #{target_url}.
          Error: #{err}"

    EarlyError.create(reportable_id: report.id,
                      report_id: report.id,
                      reportable_type: report.type,
                      description: msg,
                      sort_error: 10)
    # trans = TransHandler.trans_struct
    # trans.clazz = 'EarlyError'
    # trans.attrs = { reportable_id: report.id,
    #                 report_id: report.id,
    #                 reportable_type: report.type,
    #                 description: msg,
    #                 sort_error: 10 }
    ## WTF? create_simple is not a class method. would have to be `trans.create_simple(trans)`
    ## back out this untested crap
    # TransHandler.create_simple(trans)
  end
end
