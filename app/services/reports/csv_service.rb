# frozen_string_literal: true

require 'csv'

module Reports
  class CsvService
    attr_reader :result

    def initialize(csv_struct)
      @result = OpenStruct.new(file_name: '',
                               file_path: '',
                               error: false)
      call(csv_struct)
    end

    def call(csv_struct)
      begin
        @result.file_path = Tempfile.new([csv_struct.file_name, '.csv']).tap do |file|
          CSV.open(file, 'wb') do |csv|
            csv << csv_struct.headers

            if csv_struct.csv_data.first.is_a?(Array)
              # For EarlyError CSV
              csv_struct.csv_data.each do |row|
                csv << row
              end
            else
              # For ErrorItem CSVs
              csv_struct.csv_data.each do |row|
                csv << row.values
              end
            end
          end
        end
      rescue StandardError => e
        Rails.logger.error "Error saving csv for report id #{csv_struct.report_id}: #{e}"
        @result.error = true
      end
      @result.file_name = "#{csv_struct.file_name}.csv"
    end
  end
end
