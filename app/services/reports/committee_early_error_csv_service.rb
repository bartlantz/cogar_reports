# frozen_string_literal: true

module Reports
  class CommitteeEarlyErrorCsvService
    attr_reader :result

    def initialize(report)
      @report = report
      @result = nil
    end

    def call
      csv_data = EarlyError
                 .where(report_id: @report.id)
                 .pluck(:sort_error, :description)

      r_date = @report.created_at.strftime('%m%d%Y')

      early_error_struct = OpenStruct.new(
        report_id: @report.id,
        file_name: "early_errors__report#{@report.id}-#{r_date}",
        csv_data: csv_data,
        headers: %w[sort_error description]
      )
      @result = Reports::CsvService.new(early_error_struct).result
    end
  end
end
