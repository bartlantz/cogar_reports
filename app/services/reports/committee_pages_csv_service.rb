# frozen_string_literal: true

module Reports
  class CommitteePagesCsvService
    attr_reader :result

    def initialize(report)
      @report = report
      @result = nil
    end

    def call
      csv_data = ErrorItemsByReport.new(@report).call
      # csv_data = ErrorItemsByReportCopy.new(@report).call

      r_date = @report.created_at.strftime('%m%d%Y')
      committee_struct = OpenStruct.new(
        report_id: @report.id,
        csv_data: csv_data,
        headers: %w[error_item_id target_page committee session_year comm_abbrev meeting hearing_item
                    app_votes_hearing_item_id sort_error clics_key clics_value target_css target_value],
        file_name: "comm_pages__report#{@report.id}-#{r_date}"
      )

      @result = Reports::CsvService.new(committee_struct).result
    end
  end
end
