# frozen_string_literal: true

require 'digest'

module Committees
  class WriteHearingItemData
    def initialize(writer_struct)
      @writer_struct = writer_struct
      @report = writer_struct.report
      @committee = writer_struct.committee
      @hearing = writer_struct.hearing
      @h_data = writer_struct.h_data
    end

    def call
      do_hearing_items
    end

    def do_hearing_items
      # If we got here, the digest has changed or the data is new
      check_hearing_items

      bill_data = @h_data.dig('bill_summaries')
      bill_data.each do |b|
        hi = create_or_update_hearing_items(b)
        write_vote_summaries(hi, b) if hi.present?
      end
      nil
    end

    def check_hearing_items
      # If there is a different number of bill_summaries, just delete all the hearing_items to be safe.
      h_hi = @hearing.hearing_item_uuids.length
      hi = @hearing.hearing_items.length
      b = @h_data.dig('bill_summaries').length
      # will not work because of foreign key violation
      @hearing.hearing_items.delete_all unless h_hi == hi && b == hi
    end

    def create_or_update_hearing_items(bill_data)
      hi = HearingItem.find_by(hearing_item_uuid: bill_data.dig('hearing_item_uuid'))
      if hi.present?
        update_hi(hi, bill_data)
      else
        create_hi(bill_data)
      end
    end

    def update_hi(hi, bill_data)
      begin
        hi.update!(bill_num: bill_data.dig('bill_num'),
                   summary_title: bill_data.dig('summary_title'),
                   bill_action: bill_data.dig('bill_action'),
                   bill_summ_url: bill_data.dig('bill_summ_url'),
                   hearing: @hearing,
                   hearing_item_uuid: bill_data.dig('hearing_item_uuid'),
                   comm_report_url: bill_data.dig('comm_report_url', 0),
                   votes: bill_data.dig('vote_summaries'),
                   vote_summaries_url: bill_data.dig('vote_summaries', 0, 'url'))
      rescue ActiveRecord::RecordInvalid => e
        transaction_rescue(e, bill_data)
      end
      hi
    end

    def create_hi(bill_data)
      begin
        hi = HearingItem
             .create!(hearing_id: @hearing.id,
                      hearing_item_uuid: bill_data.dig('hearing_item_uuid'),
                      bill_num: bill_data.dig('bill_num'),
                      summary_title: bill_data.dig('summary_title'),
                      bill_action: bill_data.dig('bill_action'),
                      bill_summ_url: bill_data.dig('bill_summ_url'),
                      comm_report_url: bill_data.dig('comm_report_url', 0),
                      votes: bill_data.dig('vote_summaries'),
                      vote_summaries_url: bill_data.dig('vote_summaries', 0, 'url'))
      rescue ActiveRecord::RecordInvalid => e
        transaction_rescue(e, bill_data)
      end
      hi
    end

    def transaction_rescue(err, bill_data)
      msg = "Failed to create/update HearingItem where hearing_item_uuid: #{bill_data.dig('hearing_item_uuid')}. | Error: #{err.record.errors.full_messages}. | Additional Details: Committee #{@committee.clics_committee_id}, Hearing Item id: #{@hearing.id}. CLICS hearing_item_uuid from bill_summaries: #{bill_data.dig('hearing_item_uuid')}. DB Hearing.hearing_item_uuids: #{@hearing.hearing_item_uuids}}"

      EarlyError.create(reportable_id: @report.id,
                        report_id: @report.id,
                        reportable_type: @report.type,
                        description: msg,
                        sort_error: 1)
      nil
    end

    def write_vote_summaries(hi, bill_data)
      # need to do a length check to see if there are more or less VoteSummaries
      # in the db for some reason...
      vs = bill_data.dig('vote_summaries')
      # This only makes sense to me if you would delete_all if it's a different count
      # hi.vote_summaries.delete_all unless hi.vote_summaries.length != vs&.length
      hi.vote_summaries.delete_all if hi.vote_summaries.length != vs&.length
      vs.each do |vote_summary|
        @writer_struct.vote_summary = vote_summary
        @writer_struct.hearing_item = hi
        Committees::WriteVoteSummaryData.new(@writer_struct).call
      end
    end
  end
end
