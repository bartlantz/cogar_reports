# frozen_string_literal: true

require 'digest'

module Committees
  class GetClicsHearingData
    def initialize(report)
      @report = report
    end

    def call
      Committee.find_each do |c|
        c.meeting_uids.each do |mtg_uid|
          h_data = get_clics_hearing_data(c, mtg_uid)
          # FIXME: unless with a negative !
          #  next unless h_data.present? && !h_data.dig('error')
          #  How did rubocop allows this?
          next if h_data.blank? || h_data.dig('error')

          writer_struct = OpenStruct.new(report: @report,
                                         committee: c,
                                         h_data: h_data)
          Committees::WriteHearingData.new(writer_struct).call
        end
      end
    end

    # build hearing data and write meeting
    #  Added by Bart for debugging, not used in nightly report
    def build_hearing_data(comm, mtg_uid)
      h_data = get_clics_hearing_data(comm, mtg_uid)
      return if h_data.blank? || h_data.dig('error')

      writer_struct = OpenStruct.new(report: @report,
                                     committee: comm,
                                     h_data: h_data)
      Committees::WriteHearingData.new(writer_struct).call
    end

    def get_clics_hearing_data(committee, meeting_uid)
      # If there are no meetings (an empty array), don't log an error since it
      # could be the first week of session.
      return nil if meeting_uid == '0'

      endpoint = make_endpoint(meeting_uid)
      result = GetJson.new(endpoint).call
      handle_json_data(committee, meeting_uid, endpoint, result)
    end

    def make_endpoint(committee_id)
      endpoints = Endpoints.new
      sitename = endpoints.clics_sitename
      all_committees = endpoints.clics_summary_m_id_path

      "#{sitename}#{all_committees}#{committee_id}"
    end

    def handle_json_data(committee, meeting_uid, endpoint, result)
      return result.data if result && result.data.present?

      unknown_err = "| Details: unknown error for Committee #{committee.clics_committee_id}'s meeting_uid: #{meeting_uid}. All meeting_uids: #{committee.meeting_uids}."

      err = result && result.error.present? ? result.error : unknown_err
      msg = "Could not parse JSON from CLICS committees endpoint: #{endpoint}. | Error: #{err}"
      EarlyError.create(reportable_id: @report.id,
                        report_id: @report.id,
                        reportable_type: @report.type,
                        description: msg,
                        sort_error: 2)
      nil
    end
  end
end
