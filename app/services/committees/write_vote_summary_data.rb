# frozen_string_literal: true

require 'digest'

module Committees
  class WriteVoteSummaryData
    def initialize(writer_struct)
      @report = writer_struct.report
      @committee = writer_struct.committee
      @hearing = writer_struct.hearing
      @h_data = writer_struct.h_data
      @hearing_item = writer_struct.hearing_item
      @vote_summary = writer_struct.vote_summary
    end

    def call
      do_vote_summary
    end

    def do_vote_summary
      digest = Digest::SHA256.hexdigest(@vote_summary.to_s)
      vote_obj = VoteSummary.find_by(clics_vote_digest: digest)
      if vote_obj.present?
        update_vote(vote_obj)
      else
        create_vote(digest)
      end
    end

    def update_vote(vote_summary_obj)
      vote_summary_obj.update!(url: @vote_summary.dig('url'),
                               vote_motion: @vote_summary.dig('vote_motion'),
                               vote_time: @vote_summary.dig('vote_time'))
    rescue ActiveRecord::RecordInvalid => e
      transaction_rescue(e)
    end

    def create_vote(digest)
      VoteSummary
        .create!(clics_vote_digest: digest,
                 hearing_item_id: @hearing_item.id,
                 url: @vote_summary.dig('url'),
                 vote_motion: @vote_summary.dig('vote_motion'),
                 vote_time: @vote_summary.dig('vote_time'))
    rescue ActiveRecord::RecordInvalid => e
      transaction_rescue(e)
    end

    def transaction_rescue(err)
      msg = "Failed to create/update VoteSummary where hearing_item_uuid: #{@hearing_item.hearing_item_uuid} and bill_number is #{@hearing_item.bill_num}. | Error: #{err.record.errors.full_messages}. | Additional Details: Committee #{@committee.clics_committee_id}, Hearing id: #{@hearing.id}.  CLICS data for this object: #{@vote_summary}"

      EarlyError.create(reportable_id: @report.id,
                        report_id: @report.id,
                        reportable_type: @report.type,
                        description: msg,
                        sort_error: 1)
      nil
    end
  end
end
