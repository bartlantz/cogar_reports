# frozen_string_literal: true

module Committees
  class HearingItemTargetPages
    def initialize(report)
      @report = report
    end

    def call
      # Get the data from cogar.
      endpoint = make_endpoint
      result = fetch_cmhi_data(endpoint)
      cogar_hearing_items = handle_json_data(endpoint, result)

      # Make a hash of the id and cmhi_uuid.
      uuid_by_cogar_id = make_ids_hash(cogar_hearing_items) if cogar_hearing_items.present?
      return nil unless uuid_by_cogar_id

      # Next make Hearing Item TargetPages. Use the id to construct the url and
      # the the cmhi_uid to find the HearingItem to save as the parentable since
      # multiple VoteSummary objs are listed on one cogar page
      do_target_pages(uuid_by_cogar_id)
    end

    def fetch_cmhi_data(endpoint)
      GetJson.new(endpoint).call
    end

    def make_endpoint
      endpoints = Endpoints.new
      site_name = endpoints.cogar_sitename
      cmhi_endpoint = endpoints.cogar_cmhi_endpoint
      "#{site_name}#{cmhi_endpoint}"
    end

    def handle_json_data(endpoint, result)
      return result.data if result && result.data.present?

      err = result && result.error.present? ? result.error : 'unknown'
      msg = "Could not parse JSON from COGA hearing_item endpoint: #{endpoint}. Error: #{err}"
      EarlyError.create(reportable_id: @report.id,
                        report_id: @report.id,
                        reportable_type: @report.type,
                        description: msg,
                        sort_error: 3)
      nil
    end

    def make_ids_hash(cogar_hearing_items)
      uuids_by_ids = {}
      cogar_hearing_items.each do |chi|
        uuids_by_ids[chi['cmhi_uuid']] = chi['id']
      end
      uuids_by_ids
    end

    def do_target_pages(uuid_by_cogar_id)
      uuid_by_cogar_id.each do |uuid, id|
        # Validate that cogar has the bare minimum data we need.
        url_filepath = build_url_filepath(uuid, id)
        next unless url_filepath.present?

        db_hearing_item = get_db_hearing_item(uuid)
        update_target_page(db_hearing_item, url_filepath) if db_hearing_item.present?
      end
      nil
    end

    def build_url_filepath(uuid, id)
      url_filepath = "committee_meeting_hearing_items/#{id}/votes"
      return url_filepath unless !uuid.present? || !id.present?

      msg = "Failed to find/create TargetPage for HearingItem due to missing data. Coga CommitteeMeetingHearingItem id: #{id} and uuid: #{uuid}"
      EarlyError.create(reportable_id: @report.id,
                        report_id: @report.id,
                        reportable_type: @report.type,
                        description: msg,
                        sort_error: 3)
      nil
    end

    def get_db_hearing_item(hearing_item_uuid)
      db_hi = HearingItem
              .find_by(hearing_item_uuid: hearing_item_uuid)
      return db_hi if db_hi.present?

      # There wasn't a hearing_item in the system already from CLICS. Add it
      # anyway. Errors will be surfaced during scraping.
      hearings = Hearing.where('? = ANY (hearing_item_uuids)', hearing_item_uuid)
      return hi_early_error(hearing_item_uuid) unless hearings.present?

      hearing = hearings.first
      HearingItem.create(hearing_id: hearing.id,
                         hearing_item_uuid: hearing_item_uuid)
    end

    def hi_early_error(hearing_item_uuid)
      msg = "Failed to find/create HearingItem with hearing_item_uuid #{hearing_item_uuid} from COGA endpoint (id doesn't exist in CLICS)."

      EarlyError.create(reportable_id: @report.id,
                        report_id: @report.id,
                        reportable_type: @report.type,
                        description: msg,
                        sort_error: 3)
      nil
    end

    def update_target_page(hearing_item, url_filepath)
      tp = TargetPage.find_by(parentable_id: hearing_item.id,
                              parentable_type: 'HearingItem')
      begin
        if tp
          tp.update!(url_filepath: url_filepath)
        else
          TargetPage.create!(parentable_id: hearing_item.id,
                             parentable_type: 'HearingItem',
                             url_filepath: url_filepath)
        end
      rescue ActiveRecord::RecordInvalid => e
        transaction_rescue(e, hearing_item)
      end
    end

    def transaction_rescue(err, hearing_item)
      msg = "Failed to find/create TargetPage for Cogar CMHI id: #{hearing_item.hearing_item_uuid}. Error: #{err.record.errors.full_messages}"

      EarlyError.create(reportable_id: @report.id,
                        report_id: @report.id,
                        reportable_type: @report.type,
                        description: msg,
                        sort_error: 3)
      nil
    end
  end
end
