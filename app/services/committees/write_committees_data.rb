# frozen_string_literal: true

require 'digest'

module Committees
  class WriteCommitteesData
    def initialize(report, committees_data)
      @report = report
      @committees_data = committees_data
    end

    def call
      @committees_data.each do |c_data|
        # Create a digest of the whole committee object from CLICS
        digest = Digest::SHA256.hexdigest(c_data.to_s)

        committee = Committee.find_by(clics_lm_digest: digest)
        next if committee.present?

        write_committee(c_data, digest)
      end
    end

    def write_committee(c_data, digest)
      # We either have new changes or the object is brand-new. If there are
      # changes, just write over the existing fields since errors and
      # corresponding data are saved in an EarlyError
      c_id = c_data.dig('committee_name')
      committee = Committee.find_by(clics_committee_id: c_id)

      begin
        if committee
          committee
            .update!(meeting_dates: c_data.dig('meeting_dates'),
                     meeting_uids: c_data.dig('meeting_uids'),
                     clics_lm_digest: digest)
        else
          Committee
            .create!(clics_committee_id: c_id,
                     meeting_dates: c_data.dig('meeting_dates'),
                     meeting_uids: c_data.dig('meeting_uids'),
                     clics_lm_digest: digest)

        end
      rescue ActiveRecord::RecordInvalid => e
        transaction_rescue(e, c_id)
      end
      nil
    end

    def transaction_rescue(err, c_id)
      msg = "Failed to create/update Committee where clics_committee_id: #{c_id}. Error: #{err.record.errors.full_messages}"

      EarlyError.create(reportable_id: @report.id,
                        report_id: @report.id,
                        reportable_type: @report.type,
                        description: msg,
                        sort_error: 1)

      nil
    end
  end
end
