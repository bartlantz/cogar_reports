# frozen_string_literal: true

module Committees
  module Comparisons
    class CompareHearingItems
      include Shared

      def initialize(comparables)
        @comparables = comparables
        @ei_struct = OpenStruct
                     .new(report: comparables.report,
                          tp: comparables.tp,
                          clics_key: '',
                          clics_value: '',
                          target_css: '',
                          target_value: '',
                          sort_error: 0)
      end

      def call
        # This is already scoped to a single hearing: comparables.hearing
        compare_hearing_item_details(@comparables, @ei_struct)
      end

      def compare_hearing_item_details(comparables, ei_struct)
        # hi is the db HI object.
        # Currently, this fuctionality should be associated with a HearingItemErrorItem

        # hi_fields = %i[summary_title bill_action]
        # Kevin wants to use the bill_num field not the summary_title
        hi_fields = %i[bill_num bill_action]
        hi_fields.each do |hi_field|
          compare_fields_values(comparables, hi_field, ei_struct)
        end

        hi_fields = %i[bill_summ_url comm_report_url]
        hi_fields.each do |hi_field|
          compare_fields_presence(comparables, hi_field, ei_struct)
        end

        compare_vote_summaries_url(comparables, ei_struct)
      end

      def compare_fields_values(comparables, hi_field, ei_struct)
        doc = comparables.doc
        hearing_item = comparables.hearing_item

        ei_struct.clics_key = "bill_summaries.#{hi_field}"
        ei_struct.clics_value = hearing_item.send(hi_field)
        ei_struct.target_css = ".cwr_hearing_item_#{hi_field}"
        ei_struct.target_value = get_target_value(doc, hearing_item, hi_field)
        ei_struct.sort_error = get_sort_error(ei_struct.clics_value, ei_struct.target_value)

        ei = create_error_item(ei_struct)
        HearingItemErrorItem.create!(hearing_item_id: hearing_item.id, error_item_id: ei.id)
      end

      def compare_fields_presence(comparables, hi_field, ei_struct)
        # we can't compare the urls. But we want to display the values we scraped, and we want to write an erroritem if a value is missing.

        doc = comparables.doc
        hearing_item = comparables.hearing_item

        ei_struct.clics_key = "bill_summaries.#{hi_field}"
        ei_struct.clics_value = hearing_item.send(hi_field)
        ei_struct.target_css = ".cwr_hearing_item_#{hi_field}"
        ei_struct.target_value = get_target_value(doc, hearing_item, hi_field)
        ei_struct.sort_error = get_sort_error(ei_struct.clics_value.present?, ei_struct.target_value.present?)

        ei = create_error_item(ei_struct)
        HearingItemErrorItem.create!(hearing_item_id: hearing_item.id, error_item_id: ei.id)
      end

      def compare_vote_summaries_url(comparables, ei_struct)
        doc = comparables.doc
        hearing_item = comparables.hearing_item
        hi_field = 'vote_summaries_url'

        ei_struct.clics_key = 'bill_summaries.vote_summaries.url'
        ei_struct.clics_value = hearing_item.vote_summaries_url
        ei_struct.target_css = '.cwr_hearing_item_vote_summaries_url'
        ei_struct.target_value = get_target_value(doc, hearing_item, hi_field)
        ei_struct.sort_error = get_sort_error(ei_struct.clics_value.present?, ei_struct.target_value.present?)

        ei = create_error_item(ei_struct)
        HearingItemErrorItem.create!(hearing_item_id: hearing_item.id, error_item_id: ei.id)
      end

      def get_target_value(doc, hearing_item, hi_field)
        doc.css("##{hearing_item.hearing_item_uuid} .cwr_hearing_item_#{hi_field}").text
      end
    end
  end
end
