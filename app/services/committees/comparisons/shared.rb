# frozen_string_literal: true

module Committees
  module Comparisons
    module Shared
      def get_sort_error(clics_value, target_val)
        # Account for integers
        # Specs will fail if rubocop changes the following line of code to
        # "return 5 if clics_value.zero? && target_val.zero?" Must be "return 5
        # if clics_value == 0 && target_val == 0"

        # account for arrays/strings
        #---------------------------------------------------------------------------------
        # Do not include as an error when both are empty. If they agree it is not an error.
        #---------------------------------------------------------------------------------
        # return 5 if clics_value == 0 && target_val == 0
        # return 5 if !clics_value.present? && !target_val.present?
        return 0 if clics_value == target_val
        return 6 unless clics_value.present?
        return 7 unless target_val.present?

        8
      end

      def get_ordered_sort_error(target_value, clics_dates)
        # check to see if values are all present and return the right enum error
        # if needed
        clics_dates = [] if clics_dates == ['0']

        sort_error = get_sort_error(clics_dates, target_value)
        return sort_error if [0, 5, 6, 7].include?(sort_error)

        # Reset to zero and reformat dates if we got here
        sort_error = 0
        clics_dates = clics_dates.map do |md|
          DateTime.parse(md).strftime('%Y-%m-%d')
        rescue StandardError
          md
        end

        clics_dates.each_with_index do |clics_date, i|
          next if clics_date == target_value[i]
         
          begin
            time = Time.parse(clics_date).strftime("%H:%M %p")
          rescue StandardError
            sort_error = 9
            break
          end
          
          next if time == target_value[i]

        end

        sort_error
      end

      def check_date_or_time(a, b)
        a == b
      end

      def create_error_item(ei_struct)
        ErrorItem
          .create!(reportable_id: ei_struct.report.id,
                   report_id: ei_struct.report.id,
                   reportable_type: ei_struct.report.type,
                   target_page_id: ei_struct.tp.id,
                   clics_key: ei_struct.clics_key,
                   clics_value: ei_struct.clics_value,
                   target_css: ei_struct.target_css,
                   target_value: ei_struct.target_value,
                   missing_value: ei_struct&.missing_value,
                   sort_error: ei_struct.sort_error)
      end
    end
  end
end
