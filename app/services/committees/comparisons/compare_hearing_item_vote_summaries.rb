# frozen_string_literal: true

module Committees
  module Comparisons
    class CompareHearingItemVoteSummaries
      include Shared

      def initialize(comparables)
        # comparables is an openstruct of report, target page, nokogiri doc.
        @comparables = comparables
        @ei_struct = OpenStruct
                     .new(report: comparables.report,
                          tp: comparables.tp,
                          clics_key: '',
                          clics_value: '',
                          target_css: '',
                          target_value: '',
                          sort_error: 0)
      end

      def call
        # Check the order in which the header rows appear, and then check the value of vote_motion in 2 places
        compare_vote_summary_order
        hearing_item = @comparables.tp.parentable
        hearing_item.vote_summaries.each do |vs|
          compare_fields_values('vote_motion', vs)
          compare_fields_values('vote_motion_header', vs)
        end
      end

      def compare_vote_summary_order
        @ei_struct.clics_key = 'vote summary order: vote_summaries.vote_time'
        @ei_struct.clics_value = get_clics_vote_times
        @ei_struct.target_css = '.cwr_vote_item'
        @ei_struct.target_value = get_tp_vote_times
        @ei_struct.sort_error = get_ordered_sort_error(@ei_struct.target_value, @ei_struct.clics_value)

        ei = create_error_item(@ei_struct)
        hearing_item = @comparables.tp.parentable
        HearingItemErrorItem.create!(hearing_item_id: hearing_item.id, error_item_id: ei.id)
      end

      def get_clics_vote_times
        hi = HearingItem.find(@comparables.tp.parentable_id)
        return [] unless hi.votes.present?

        votes = hi.votes
        votes.map do |v|
          vt = v.dig('vote_time')
          begin
            date_obj = DateTime.strptime(vt, '%m/%d/%Y %I:%M:%S %p')
            date_obj.strftime('%m/%d/%Y %I:%M %p')
          rescue StandardError
            vt
          end
        end
      end

      def get_tp_vote_times
        target_value = @comparables.doc.css('.cwr_vote_time')
        target_value.map(&:text)
      end

      def compare_fields_values(hi_field, vote_summary_obj)
        # There are two places to check for motion text, differentiated by a css
        # classname. Check the doc, then strip the suffix from the css class if
        # it exists before moving on
        @ei_struct.target_css = ".cwr_#{hi_field}"
        hi_field = hi_field.gsub('_header', '')

        @ei_struct.clics_key = "vote_summaries.#{hi_field}"
        @ei_struct.clics_value = vote_summary_obj.send(hi_field)
        @ei_struct.target_value = get_target_value(vote_summary_obj, hi_field)
        @ei_struct.sort_error = get_sort_error(@ei_struct.clics_value, @ei_struct.target_value)

        ei = create_error_item(@ei_struct)
        VoteSummaryErrorItem.create!(vote_summary_id: vote_summary_obj.id, error_item_id: ei.id)
      end

      def get_target_value(vote_summary_obj, hi_field)
        vt = vote_summary_obj.vote_time
        # Reduce liklihood of parsing-errors by checking string length
        return nil unless vt.length == 22 || vt.length == 16

        begin
          date_obj = DateTime.strptime(vt, '%m/%d/%Y %I:%M:%S %p')
          squished_time = date_obj.strftime('%Y%m%d%H%M%S')
          @comparables.doc.css("#cwr_#{squished_time} .cwr_#{hi_field}").text
        rescue StandardError => e
          Rails.logger.error e
          Rails.logger.error vt
          nil
        end
      end
    end
  end
end
