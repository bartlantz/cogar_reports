# frozen_string_literal: true

module Committees
  module Comparisons
    class CompareHearings
      include Shared

      def initialize(comparables)
        @comparables = comparables
        @ei_struct = OpenStruct
                     .new(report: comparables.report,
                          tp: comparables.tp,
                          clics_key: '',
                          clics_value: '',
                          target_css: '',
                          target_value: '',
                          missing_value: '',
                          sort_error: 0)
      end

      def call
        # This is already scoped to a single hearing: comparables.hearing
        compare_hearing_items_count(@comparables, @ei_struct)
        compare_hearing_items_order(@comparables, @ei_struct)
      end

      def compare_hearing_items_count(comparables, ei_struct)
        # Currently, this fuctionality should be associated with a HearingErrorItem.
        # Per hearing, count and compare the total and order of hearing_items per hearing.

        doc = comparables.doc
        hearing = comparables.hearing

        ei_struct.clics_key = 'hearing_items count'
        ei_struct.clics_value = hearing.hearing_item_uuids.length
        ei_struct.target_css = ".cwr_hearing_#{hearing.meeting_uid}"
        ei_struct.target_value = get_target_value_count(doc, hearing)
        ei_struct.sort_error = get_sort_error(ei_struct.clics_value, ei_struct.target_value)

        ei = create_error_item(ei_struct)
        HearingErrorItem.create!(hearing_id: hearing.id, error_item_id: ei.id)
      end

      def get_target_value_count(doc, hearing)
        target_val = doc.css(".cwr_hearing_#{hearing.meeting_uid} .cwr_hearing_item_uuids")
        target_val.length
      end

      def compare_hearing_items_order(comparables, ei_struct)
        doc = comparables.doc
        hearing = comparables.hearing

        ei_struct.clics_key = 'hearing_items order: bill_summaries.hearing_item_uuid'
        ei_struct.clics_value = hearing.hearing_item_uuids
        ei_struct.target_css = '.cwr_hearing_item_uuids'
        ei_struct.target_value = get_target_values(doc, hearing)
        ei_struct.sort_error = get_sort_error(ei_struct.clics_value, ei_struct.target_value)

        ei = create_error_item(ei_struct)
        HearingErrorItem.create!(hearing_id: hearing.id, error_item_id: ei.id)
      end

      def get_target_values(doc, hearing)
        target_val = doc.css(".cwr_hearing_#{hearing.meeting_uid} .cwr_hearing_item_uuids")
        target_val.map do |row|
          row['id']
        end
      end
    end
  end
end
