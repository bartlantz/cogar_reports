# frozen_string_literal: true

require 'digest'

module Committees
  class WriteHearingData
    def initialize(writer_struct)
      @writer_struct = writer_struct
      @report = writer_struct.report
      @committee = writer_struct.committee
      @h_data = writer_struct.h_data
    end

    def call
      digest = Digest::SHA256.hexdigest(@h_data.to_s)
      hearing = Hearing.find_by(clics_m_id_digest: digest)
      # If a hearing exists and the digest hasn't changed, move on
      return nil if hearing.present?

      hearing = check_hearing(digest)
      comm_hearing = make_committee_hearing(@committee.id, hearing.id)
      # If there is no comm_hearing, don't move on
      return nil unless comm_hearing.present?

      @writer_struct.hearing = hearing
      Committees::WriteHearingItemData.new(@writer_struct).call
    end

    def check_hearing(digest)
      hearing_item_uuids = get_hearing_items
      create_or_update_hearing(digest, hearing_item_uuids)
    end

    def get_hearing_items
      hearing_item_uuids = []
      bill_summaries = @h_data.dig('bill_summaries')
      if bill_summaries.present?
        hearing_item_uuids = bill_summaries.map do |bs|
          bs.dig('hearing_item_uuid')
        end
      end
      hearing_item_uuids.compact
    end

    def create_or_update_hearing(digest, hearing_item_uuids)
      begin
        hearing = Hearing
                  .find_by(meeting_uid: @h_data.dig('meeting_uid'))
        if hearing.present?
          hearing.update!(clics_m_id_digest: digest,
                          meeting_date: @h_data.dig('meeting_date'),
                          meeting_time: @h_data.dig('meeting_time'),
                          hearing_item_uuids: hearing_item_uuids)
        else
          hearing = Hearing.create!(clics_m_id_digest: digest,
                                    meeting_uid: @h_data.dig('meeting_uid'),
                                    meeting_date: @h_data.dig('meeting_date'),
                                    meeting_time: @h_data.dig('meeting_time'),
                                    hearing_item_uuids: hearing_item_uuids)
        end
      rescue ActiveRecord::RecordInvalid => e
        transaction_rescue(e)
      end
      hearing
    end

    def transaction_rescue(err)
      msg = "Failed to create/update Hearing where meeting uid: #{@h_data.dig('meeting_uid')}. Error: #{err.record.errors.full_messages}"

      EarlyError.create(reportable_id: @report.id,
                        report_id: @report.id,
                        reportable_type: @report.type,
                        description: msg,
                        sort_error: 1)
    end

    def make_committee_hearing(committee_id, hearing_id)
      return nil if !committee_id.present? || !hearing_id.present?

      CommitteeHearing
        .find_or_create_by(committee_id: committee_id,
                           hearing_id: hearing_id)
    end
  end
end
