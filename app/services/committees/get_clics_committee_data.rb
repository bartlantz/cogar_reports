# frozen_string_literal: true

require 'digest'

module Committees
  class GetClicsCommitteeData
    def initialize(report)
      @report = report
    end

    def call
      endpoint = make_endpoint
      result = GetJson.new(endpoint).call
      handle_json_data(endpoint, result)
    end

    def make_endpoint
      endpoints = Endpoints.new
      sitename = endpoints.clics_sitename
      all_committees = endpoints.clics_summary_lm_path

      # TODO: MAKE SURE TO CHANGE THIS TO 2015 FOR LIVE TESTS
      # start_date = endpoints.session_start_date_2021a
      # start_date = endpoints.session_start_date_2019a
      start_date = endpoints.session_start_date_2015a

      "#{sitename}#{all_committees}#{start_date}"
    end

    def handle_json_data(endpoint, result)
      return result.data if result && result.data.present?

      err = result && result.error.present? ? result.error : 'unknown'
      msg = "Could not parse JSON from CLICS committees endpoint: #{endpoint}. | Error: #{err}"
      EarlyError.create(reportable_id: @report.id,
                        report_id: @report.id,
                        reportable_type: @report.type,
                        description: msg,
                        sort_error: 2)
      nil
    end

    def write_committees(c_data)
      Committees::WriteCommitteesData.new(@report, c_data).call
    end
  end
end
