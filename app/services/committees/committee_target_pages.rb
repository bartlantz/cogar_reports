# frozen_string_literal: true

module Committees
  class CommitteeTargetPages
    def initialize(report)
      @report = report
    end

    def call
      endpoint = make_endpoint
      result = GetJson.new(endpoint).call
      committees = handle_json_data(endpoint, result)
      validate_existing_target_pages(committees) if committees.present?
    end

    def handle_json_data(endpoint, result)
      return result.data if result && result.data.present?

      err = result && result.error.present? ? result.error : 'unknown'
      msg = "Could not parse JSON from COGA committees endpoint: #{endpoint}. Error: #{err}"
      EarlyError.create(reportable_id: @report.id,
                        report_id: @report.id,
                        reportable_type: @report.type,
                        description: msg,
                        sort_error: 3)
      nil
    end

    def make_endpoint
      endpoints = Endpoints.new
      site_name = endpoints.cogar_sitename
      committees = endpoints.cogar_committees_endpoint
      "#{site_name}#{committees}?sess=all"
    end

    def validate_existing_target_pages(committees)
      committees.each do |c|
        url_filepath = build_filepath(c)
        next unless url_filepath.present?

        db_committee = get_db_committee(c.dig('committee_uuid'))
        update_target_page(db_committee, url_filepath, c.dig('slug'))
      end
      nil
    end

    def build_filepath(c)
      short_name = c.dig('session', 'short_name')
      chamber = TargetPage.interim_or_yearround(c.dig('chamber'))
      slug = c.dig('slug')

      url_filepath = "committees/#{short_name}/#{chamber}/#{slug}"

      if !short_name || !chamber || !slug

        msg = "Failed to find/create TargetPage for Committee #{c.dig('committee_uuid')} due to bad filepath.
        Session short_name: #{short_name}, chamber: #{c.dig('chamber')}, and/or slug: #{slug}"

        EarlyError.create(reportable_id: @report.id,
                        report_id: @report.id,
                          reportable_type: @report.type,
                          description: msg,
                          sort_error: 3)

        url_filepath = nil
      end
      url_filepath
    end

    def get_db_committee(committee_uuid)
      db_committee = Committee
                     .find_by(clics_committee_id: committee_uuid)
      return db_committee if db_committee.present?

      # There wasn't a committee in the system already from CLICS. Add it
      # anyway. Errors will be surfaced during scraping. The digest can't be
      # blank, so create a bogus one. If there is ever real clics data for the
      # committee, it is handled in another job.
      digest = Digest::SHA256.hexdigest(committee_uuid.to_s)
      Committee.create!(clics_committee_id: committee_uuid,
                        meeting_dates: [0],
                        meeting_uids: [0],
                        clics_lm_digest: digest)
    end

    def update_target_page(db_committee, url_filepath, slug)
      tp = TargetPage.find_by(parentable_id: db_committee.id,
                              parentable_type: 'Committee')
      begin
        if tp
          tp.update!(target_slug: slug,
                     url_filepath: url_filepath)
        else
          TargetPage.create!(parentable_id: db_committee.id,
                             parentable_type: 'Committee',
                             target_slug: slug,
                             url_filepath: url_filepath)
        end
      rescue ActiveRecord::RecordInvalid => e
        msg = "Failed to find/create TargetPage for slug: #{slug}. Error: #{e.record.errors.full_messages}"

        EarlyError.create(reportable_id: @report.id,
                        report_id: @report.id,
                          reportable_type: @report.type,
                          description: msg,
                          sort_error: 1)
      end
      nil
    end
  end
end
