# frozen_string_literal: true

# require 'net/https'
# require 'open-uri'
require 'nokogiri'

module Committees
  class ScrapeHearingItemVoteSummaryPages
    def initialize(report)
      @report = report
      @comparables = OpenStruct.new(report: @report,
                                    tp: nil,
                                    doc: nil)
    end

    def call
      TargetPage.where(parentable_type: 'HearingItem').find_each do |tp|
        target_url = make_url(tp.url_filepath)
        result = Scraper.new.call(@report, target_url)
        next if result.error

        @comparables.tp = tp
        @comparables.doc = result.doc
        Committees::Comparisons::CompareHearingItemVoteSummaries.new(@comparables).call
      end
    end

    def make_url(target_url)
      endpoints = Endpoints.new
      sitename = endpoints.cogar_sitename

      "#{sitename}#{target_url}"
    end
  end
end
