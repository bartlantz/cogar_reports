# frozen_string_literal: true

require 'net/https'
require 'open-uri'
require 'nokogiri'

module Committees
  class ScrapeCommitteePages
    def initialize(report)
      @report = report
      @comparables = OpenStruct.new(report: @report,
                                   tp: nil,
                                   doc: nil,
                                   hearing: nil,
                                   hearing_item: nil)

      call(@report, @comparables)
    end

    def call(_report, comparables)
      TargetPage.where(parentable_type: 'Committee').find_each do |tp|
        target_url = make_url(tp.url_filepath)
        result = Scraper.new.call(@report, target_url)
        next if result.error

        comparables.tp = tp
        comparables.doc = result.doc

        Committees::Comparisons::CompareCommittees.new(comparables).call
        next unless tp.parentable.present? && tp.parentable.hearings.present?

        tp.parentable.hearings.each do |hearing|
          next unless hearing.hearing_items.present?

          comparables.hearing = hearing
          Committees::Comparisons::CompareHearings.new(comparables).call

          hearing.hearing_items.each do |hi|
            comparables.hearing_item = hi
            Committees::Comparisons::CompareHearingItems.new(comparables).call
          end
        end
      end
    end

    def make_url(target_url)
      endpoints = Endpoints.new
      sitename = endpoints.cogar_sitename

      "#{sitename}#{target_url}"
    end
  end
end
