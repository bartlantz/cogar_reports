# frozen_string_literal: true

class DigestifyErrors
  def initialize(report)
    call(report)
  end

  def call(report)
    early_errors(report)

    errs_by_committee(report)
    errs_by_hearing(report)
    errs_by_hearing_item(report)
  end

  def err_attrs(err)
    attrs = err.attributes
    attrs.delete('id')
    attrs.delete('reportable_id')
    attrs.delete('report_id')
    attrs.delete('reportable_type')
    attrs.delete('created_at')
    attrs.delete('updated_at')
    attrs.delete('err_digest')
    attrs
  end

  def early_errors(report)
    EarlyError
      .where(reportable_id: report.id)
      .find_each do |err|
        attrs = err_attrs(err)
        err.update!(err_digest: Digest::SHA256.hexdigest(attrs.to_s))
      end
  end

  def errs_by_committee(report)
    ErrorItem
      .joins(:committee_error_item)
      .where(reportable_id: report.id)
      .find_each do |err|
        attrs = err_attrs(err)
        attrs['committee'] = err.committee_error_item.committee.clics_committee_id
        err.update!(err_digest: Digest::SHA256.hexdigest(attrs.to_s))
      end
  end

  def errs_by_hearing(report)
    ErrorItem
      .joins(:hearing_error_item)
      .where(reportable_id: report.id)
      .find_each do |err|
        attrs = err_attrs(err)
        attrs['hearing'] = err.hearing_error_item.hearing.meeting_uid
        err.update!(err_digest: Digest::SHA256.hexdigest(attrs.to_s))
      end
  end

  def errs_by_hearing_item(report)
    ErrorItem
      .joins(:hearing_item_error_item)
      .where(reportable_id: report.id)
      .find_each do |err|
        attrs = err_attrs(err)
        attrs['hearing_item'] = err.hearing_item_error_item.hearing_item.hearing_item_uuid
        err.update!(err_digest: Digest::SHA256.hexdigest(attrs.to_s))
      end
  end
end
