# frozen_string_literal: true

require 'net/http'
require 'json'

class GetJson
  def initialize(endpoint)
    @endpoint = endpoint
    @result = OpenStruct.new(data: nil, error: nil)
  end

  def call
    begin
      uri = URI(@endpoint)
      response = Net::HTTP.get(uri)

      @result.data = JSON.parse(response)
    rescue StandardError => e
      msg = "Parsing error for endpoint: #{@endpoint}. Error: #{e}"
      Rails.logger.error msg
      @result.error = msg
    ensure
      # Ensure & return to make sure that the job continues...even though
      # Rubocop doesn't like it.
      return @result
    end
    @result
  end
end

# Do this so that I can initialize one of these during the initial Kickoff and then pass the one instance around (and therefore pass around an instance that I can more easily mock in a test).
# class GetJson
#   def initialize
#     @result = OpenStruct.new(data: nil, error: nil)
#   end

#   def call(endpoint)
#     begin
#       uri = URI(endpoint)
#       response = Net::HTTP.get(uri)

#       @result.data = JSON.parse(response)
#     rescue StandardError => e
#       msg = "Parsing error for endpoint: #{endpoint}. Error: #{e}"
#       Rails.logger.error msg
#       @result.error = msg
#     ensure
#       # Ensure & return to make sure that the job continues...even though
#       # Rubocop doesn't like it.
#       return @result
#     end
#     @result
#   end
# end
