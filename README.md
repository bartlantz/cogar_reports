# README

This application may be a temporary application if we find that Rspec tests and Importer Tests (tests of the CLICS-to-NewCOGAsite Importers) are sufficient.

For now, the goal of this application is to run a test once a day to verify that certain data from CLICS is being displayed correctly in the re-written COGA site. This data will be available as a web page and will be downloadable as a CSV.


## Ruby version
Ruby 2.6.x, Rails 6.x

## System dependencies
Make sure that you have:
* the versions of Ruby and Rails installed (via rbenv if there are other apps on the server)
* Postgres(>10) installed and set up with a user.
* installed or updated Yarn (not npm)



## Initial Set Up

### Bundle-managed Dependencies
* Run `bundle install` and resolve any dependencies/issues. You may need to run this again after the dependencies you'll be installing with yarn (in the next step)


### Yarn Dependencies
This installation folows the recommendations from Modern Front End Rails Development by Noel Rappin
* Webpack and Webpacker might not be installed until you run 'bundle install', so you may need to do that first.

* Run `yarn install` to make sure that you have yarn and that you have the basic packages from this repo installed. The dependencies should have already been set up in the package.json and yarn.lock files.

To addresss dependency issues, you may need to upgrade webpack and popper. If so run these commands if neded:
yarn upgrade webpack@^5.0.0
yarn add @popperjs/core@2.9.2

## Configuration
The database.yml, credentials.yml.enc, and master.key are all gitignored. Request these from a co-worker (make sure to get the un-encrypetd contents of credentials.yml.enc)


Once you have these, perform the following steps:
1. Add the master.key and database.yml to your config directory. Do not add the credentials file.
1. In terminal, run `EDITOR='vim' rails credentials:edit` which will bring up a new credentials file in vim. It will have a copy of your unique secret key. Below the key, use yml syntax to add the k/v pairs for your local version of postgres.

  ```
  development:
    dev_db: cogar_reports_dev
    dev_db_user: someusername
    dev_db_password: someps

  production:
    dev_db: cogar_reports_prod
    dev_db_user: someusername
    dev_db_password: someps
  ```

Finally, run `rails db:create` to create the initial databases, then rails `db:migrate`. At this time there is no data to seed.


## Cron Jobs
There is currently only one cron job in `config/schedule.rb`.

If you change the time or details, you may need to restart the app, but you will also need to notify the server. At the terminal, run `whenever --update-crontab`.

For more info, see https://medium.com/coffee-and-codes/schedule-tasks-using-whenever-gem-ruby-on-rails-50b9af025607.



## Test Suite
To run the test suite, run `rspec` or `bundle exec rspec`



## Hotwire and StimulusJS
Since some of the Hotwire and Stimulus features are new to us at LIS, here is some info about how a few things work in this app.



### Lazy Loading
As an example, here is how the reports#show page workss:


First Page Load:

* #show controller method, goes to #show view.
    * green & blue sections are rendered. gold section's title (with current filter params) is loaded. No Results section is loaded
    * gold section's turbo_frame_tag sends a request to #stats controller method, and re-renders the gold section's inner html as the table from the #stats View.

Filtered:

* user selects filter options and clicks button. Request is sent to the #filtered controller method, and renders the #filtered View. The view calls the same partial as #show so that...
    * green section is rendered. blue section is rendered. gold section's title (with current filter params) is loaded.
    * gold section's turbo_frame_tag sends a request to #stats controller method, and re-renders the gold section's inner html as the table from the #stats View.
        * Results Section is loaded (title only)
        * results section has a turbo_frame_tag for both error sections.
            * EE turbo_frame_tag sends a request to the #filtered_ee controller method, and re-renders the inner html with the table from a TBD #filtered_ee View
            * Same for EI section.


## Glossary for Error Types

### Comparison Errors: what do they mean?

* None
    * in dropdown to say "Do not show comparison errors"
* ok
    * not an error
* coga page: application error
    * 500 error on coga
* clics api: json empty
    * apparently this means the website has a value but the CLICS api is empty. ie CLICS originally had a value then removed it.
* clics and coga empty values
    * who knows ??  CLICS and website agree so must be an error
* clics empty value
    * cogar has value, but clics does not.
    * Clics unpublished or deleted field.
* coga empty value
    * clics has value, coga is empty
* coga mismatched value
    * different value in coga
* coga incorrect order or missing values
    * This is the fall-through error, nothing else fit.

