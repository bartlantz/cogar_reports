# frozen_string_literal: true

ActionController::Renderers.add :csv do |obj, options|
  filename = options[:filename] || 'data'
  str = obj.respond_to?(:to_csv) ? obj.to_csv : obj.to_s
  send_data str, type: Mime[:csv],
                 disposition: "attachment; filename=#{filename}.csv"
end

# from https://api.rubyonrails.org/v6.1.3/classes/ActionController/Renderers.html
# To use the csv renderer in a controller action:

# def show
#   @csvable = Csvable.find(params[:id])
#   respond_to do |format|
#     format.html
#     format.csv { render csv: @csvable, filename: @csvable.name }
#   end
# end
