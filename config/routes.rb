# frozen_string_literal: true

Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  root 'reports#index'

  resources :reports, only: :index

  resources :reports, only: :show do
    get 'stats', to: 'reports#stats'
    get 'filtered', to: 'reports#filtered'
    get 'filtered_ee', to: 'reports#filtered_ee'
    get 'filtered_ei', to: 'reports#filtered_ei'
  end

  scope module: :reports do
    get 'download_committee_ei', to: 'downloads#download_committee_ei', param: :report_id
    get 'download_committee_ee', to: 'downloads#download_committee_ee', param: :report_id
  end

end
