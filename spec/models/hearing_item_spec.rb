# frozen_string_literal: true

# == Schema Information
#
# Table name: hearing_items
#
#  id                 :bigint           not null, primary key
#  bill_action        :string
#  bill_num           :string
#  bill_summ_url      :string
#  comm_report_url    :string
#  hearing_item_uuid  :string
#  summary_title      :string
#  vote_summaries_url :string
#  votes              :jsonb
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  hearing_id         :bigint
#
# Indexes
#
#  index_hearing_items_on_hearing_id  (hearing_id)
#
# Foreign Keys
#
#  fk_rails_...  (hearing_id => hearings.id)
#
require 'rails_helper'

RSpec.describe HearingItem, type: :model do
  context 'Validations' do
    let!(:committee) do
      Committee.create!(clics_lm_digest: 'somestringthatisahash',
                        meeting_dates: %w[date1 date2],
                        meeting_uids: %w[mtg1 mtg2],
                        clics_committee_id: 'somestringfromclics')
    end
    let!(:hearing) do
      Hearing.create!(
        clics_m_id_digest: 'somestringhash',
        meeting_uid: 'anotherclicsuuid'
      )
    end

    it 'is invalid without all 3 validated fields' do
      expect(HearingItem
              .new(hearing_id: hearing.id)).to_not be_valid
      expect(HearingItem
              .new(hearing_item_uuid: 'someclicsuuid')).to_not be_valid
    end

    it 'is valid with all validated fields' do
      expect(HearingItem
              .new(hearing_id: hearing.id,
                   hearing_item_uuid: 'someclicsuuid')).to be_valid
    end
  end

  context 'Methods' do
  end
end
