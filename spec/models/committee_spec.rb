# frozen_string_literal: true

# == Schema Information
#
# Table name: committees
#
#  id                 :bigint           not null, primary key
#  abbreviation       :string
#  clics_lm_digest    :string
#  meeting_dates      :string           default([]), is an Array
#  meeting_uids       :string           default([]), is an Array
#  session_year       :string
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  clics_committee_id :string
#
require 'rails_helper'

RSpec.describe Committee, type: :model do
  context 'Validations' do
    it 'is invalid without all  validated fields' do
      expect(Committee
              .new(clics_committee_id: 'somestringfromclics')).to_not be_valid
      expect(Committee
              .new(clics_lm_digest: 'somestringthatisahash')).to_not be_valid
    end

    it 'validates uniqueness for clics_lm_digest' do
      Committee.create!(clics_lm_digest: 'THISISTHESAME',
                        clics_committee_id: 'somestringfromclics')
      expect(Committee
              .new(clics_lm_digest: 'THISISTHESAME',
                   clics_committee_id: 'somestringfromclics2')).to_not be_valid
    end

    it 'validates uniqueness for clics_committee_id' do
      Committee.create!(clics_lm_digest: 'somestringthatisahash',
                        meeting_dates: %w[date1 date2],
                        meeting_uids: %w[mtg1 mtg2],
                        clics_committee_id: 'THISISTHESAME')
      expect(Committee
              .new(clics_lm_digest: 'somestringthatisahash2',
                   meeting_dates: %w[date11 date2],
                   meeting_uids: %w[mtg11 mtg2],
                   clics_committee_id: 'THISISTHESAME')).to_not be_valid
    end

    it 'is valid with all validated fields' do
      expect(Committee
              .new(clics_lm_digest: 'somestringthatisahash',
                   clics_committee_id: 'somestringfromclics')).to be_valid
    end
  end

  context 'Methods' do
  end
end
