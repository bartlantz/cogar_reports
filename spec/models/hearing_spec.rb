# frozen_string_literal: true

# == Schema Information
#
# Table name: hearings
#
#  id                 :bigint           not null, primary key
#  clics_m_id_digest  :string
#  hearing_item_uuids :string           default([]), is an Array
#  meeting_date       :string
#  meeting_time       :string
#  meeting_uid        :string
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#
require 'rails_helper'

RSpec.describe Hearing, type: :model do
  context 'Validations' do
    let!(:committee) do
      Committee.create!(clics_lm_digest: 'somestringthatisahash',
                        meeting_dates: %w[date1 date2],
                        meeting_uids: %w[mtg1 mtg2],
                        clics_committee_id: 'somestringfromclics')
    end

    it 'is invalid without all validated fields' do
      expect(Hearing
              .new(meeting_uid: 'anotherclicsuuid')).to_not be_valid

      expect(Hearing
              .new(clics_m_id_digest: 'somestringhash')).to_not be_valid
    end

    it 'validates clics_m_id_digest is unique' do
      Hearing.create!(clics_m_id_digest: 'THISISTHESAME',
                      meeting_uid: 'anotherclicsuuid')
    end

    it 'is valid with all validated fields' do
      expect(Hearing
              .new(
                clics_m_id_digest: 'somestringhash',
                meeting_uid: 'anotherclicsuuid'
              )).to be_valid
    end
  end

  context 'Methods' do
  end
end
