# frozen_string_literal: true

# == Schema Information
#
# Table name: error_items
#
#  id              :bigint           not null, primary key
#  clics_key       :string
#  clics_value     :text
#  err_digest      :string
#  reportable_type :string
#  sort_error      :integer
#  target_css      :string
#  target_value    :text
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  reportable_id   :bigint
#  target_page_id  :bigint
#
# Indexes
#
#  index_error_items_on_reportable      (reportable_type,reportable_id)
#  index_error_items_on_target_page_id  (target_page_id)
#
# Foreign Keys
#
#  fk_rails_...  (target_page_id => target_pages.id)
#
require 'rails_helper'

RSpec.describe ErrorItem, type: :model do
  # TODO: needs TargetPage (and its committee)
  context 'Validations' do
    let!(:report) { Reports::CommitteeReport.create }

    xit 'is invalid without all 2 validated fields' do
      expect(ErrorItem
              .new(reportable_id: report.id)).to_not be_valid
      expect(ErrorItem
              .new(sort_error: 1)).to_not be_valid
    end

    xit 'is valid with all 2 validated fields' do
      expect(ErrorItem
              .new(reportable_id: report.id,
                   reportable_type: report.type,
                   sort_error: 1)).to be_valid
    end
  end

  context 'Methods' do
  end
end
