# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'User view correctly ordered Results on the Show Page', type: :feature, js: true do
  let!(:report) { Reports::CommitteeReport.create! }

  it 'renders in the right order' do
    # This spec needs to test that all the errors (both kinds) for various
    # years show up in the right order in the Results section. We need several
    # kinds of committees and different years for each of them. We also need
    # to make sure that some of them share a hearing and have the correct
    # committee listed. We also need to check that the the order of hearings
    # and hearing items is tested.
    # final order:3
    make_error_items_for_1comm_1hearing_1hi(report, 1, 'H', 'APP', '2021A')
    # final order:1
    make_error_items_for_1comm_1hearing_1hi(report, 2, 'H', 'APP', '2021B')
    # final order:9
    make_error_items_for_1comm_1hearing_1hi(report, 2, 'H', 'APP', '2016B')
    # final order:2
    make_error_items_for_1comm_1hearing_1hi(report, 1, 'H', 'AGL', '2021A')
    # final order:4
    make_error_items_for_1comm_1hearing_1hi(report, 1, 'H', 'TEST', '2021A')
    # make S_APP_2021A share a hearing and hearing_item with H_APP_2021A
    # final order:8
    make_error_items_for_1comm_1hearing_1hi(report, 1, 'S', 'APP', '2021A')
    Hearing.last.update(meeting_uid: Hearing.first.meeting_uid)
    HearingItem.last.update(hearing_item_uuid: HearingItem.first.hearing_item_uuid)
    # final order:7
    make_error_items_for_1comm_1hearing_1hi(report, 1, 'S', 'AGL', '2021A')
    # final order:6
    make_error_items_for_1comm_1hearing_1hi(report, 1, 'J', 'CDC', '2021A')
    # final order:5
    make_error_items_for_1comm_1hearing_1hi(report, 1, 'I', 'ED', '2021A')
    # 2,4,1,5,9,8,7,6,3

    visit report_path(report.id)

    unselect('None', from: 'early_sort_errors')
    select('All', from: 'early_sort_errors')

    unselect('None', from: 'comparison_sort_errors')
    select('coga mismatched value', from: 'comparison_sort_errors')

    unselect('2021a', from: 'sessions')
    select('All', from: 'sessions')

    waste_seconds(3)

    within '#multi-selection-section' do
      click_on 'Filter'
    end

    # Assert correct order of Target Page sections
    target_page_order = page.all('.target-page-url').map(&:text)
    expect(target_page_order[0]).to have_content(TargetPage.find(2).url_filepath)
    expect(target_page_order[1]).to have_content(TargetPage.find(4).url_filepath)
    expect(target_page_order[2]).to have_content(TargetPage.find(1).url_filepath)
    expect(target_page_order[3]).to have_content(TargetPage.find(5).url_filepath)
    expect(target_page_order[4]).to have_content(TargetPage.find(9).url_filepath)
    expect(target_page_order[5]).to have_content(TargetPage.find(8).url_filepath)
    expect(target_page_order[6]).to have_content(TargetPage.find(7).url_filepath)
    expect(target_page_order[7]).to have_content(TargetPage.find(6).url_filepath)
    expect(target_page_order[8]).to have_content(TargetPage.find(3).url_filepath)

    # save_and_open_page

    # Assert correct order of rows within tables per committee
    c = Committee.find(1)
    all_rows = page.all("#table-#{c.clics_committee_id} tr")
    expect(all_rows.length).to eq(4)
    expect(all_rows[1].text).to have_text('H_APP_2021A - -', count: 1)
    expect(all_rows[2].text).to have_text('H_APP_2021A (H_APP_2021A-hearing1) -', count: 1)
    expect(all_rows[3].text).to have_text('H_APP_2021A (H_APP_2021A-hearing1) (H_APP_2021A-hearing1-hi1)', count: 1)

    # Note that this Committee shares a hearing and hearing item with the House
    c = Committee.find(6)
    all_rows = page.all("#table-#{c.clics_committee_id} tr")
    expect(all_rows.length).to eq(4)
    expect(all_rows[1].text).to have_text('S_APP_2021A - -', count: 1)
    expect(all_rows[2].text).to have_text('S_APP_2021A (H_APP_2021A-hearing1) -', count: 1)
    expect(all_rows[3].text).to have_text('S_APP_2021A (H_APP_2021A-hearing1) (H_APP_2021A-hearing1-hi1)', count: 1)
  end
end
