# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ErrorItemsByReport do
  describe 'Methods' do
    let!(:report) { Reports::CommitteeReport.create! }
    let!(:report2) { Reports::CommitteeReport.create! }
    let!(:querier) { ErrorItemsByReport.new(report) }

    it 'call' do
      make_error_items_for_1comm_1hearing_1hi_1vs(report, 1, 'H', 'AAA', '2016B')

      results = querier.call
      expect(results.length).to eq(5)
      expect(results[0]['clics_committee_id']).to eq('H_AAA_2016B')
      expect(results[0]['clics_key']).to eq('committee_name')
      expect(results[0]['clics_value']).to eq('H_AAA_2016B')
      expect(results[0]['sort_error']).to eq('coga mismatched value')

      expect(results[1]['clics_committee_id']).to eq('H_AAA_2016B')
      expect(results[1]['clics_key']).to eq('hearing_items count')
      expect(results[1]['clics_value']).to eq('H_AAA_2016B-hearing1')
      expect(results[1]['sort_error']).to eq('coga mismatched value')

      expect(results[2]['clics_committee_id']).to eq('H_AAA_2016B')
      expect(results[2]['clics_key']).to eq('bill_summaries.comm_report_url')
      expect(results[2]['clics_value']).to eq('H_AAA_2016B-hearing1-hi1')
      expect(results[2]['sort_error']).to eq('coga mismatched value')

      expect(results[3]['clics_committee_id']).to eq('H_AAA_2016B')
      expect(results[3]['clics_key']).to eq('vote summary order: vote_summaries.vote_time')
      expect(results[3]['clics_value']).to eq('["02/25/2021 08:44 AM", "02/25/2021 09:45 AM", "02/25/2021 10:58 AM"]')
      expect(results[3]['sort_error']).to eq('coga mismatched value')

      expect(results[4]['clics_committee_id']).to eq('H_AAA_2016B')
      expect(results[4]['clics_key']).to eq('vote_summaries.vote_motion')
      expect(results[4]['clics_value']).to eq('Adopt amendment')
      expect(results[4]['sort_error']).to eq('coga mismatched value')
    end

    it 'replace_enum_with_string(ordered_set)' do
      make_error_items_for_1comm_1hearing_1hi_1vs(report, 1, 'H', 'AAA', '2016B')

      results = querier.ordered_error_items
      results = querier.replace_enum_with_string(results)
      expect(results.length).to eq(5)
      expect(results[0]['sort_error']).to eq('coga mismatched value')
      expect(results[1]['sort_error']).to eq('coga mismatched value')
      expect(results[2]['sort_error']).to eq('coga mismatched value')
      expect(results[3]['sort_error']).to eq('coga mismatched value')
      expect(results[4]['sort_error']).to eq('coga mismatched value')
    end

    it 'ordered_error_items' do
      make_error_items_for_1comm_1hearing_1hi_1vs(report, 1, 'H', 'AAA', '2016B')
      make_error_items_for_1comm_1hearing_1hi_1vs(report, 1, 'H', 'BBB', '2016B')
      make_error_items_for_1comm_1hearing_1hi_1vs(report, 1, 'H', 'AAA', '2021A')
      make_error_items_for_1comm_1hearing_1hi_1vs(report, 1, 'H', 'BBB', '2021A')
      make_error_items_for_1comm_1hearing_1hi_1vs(report, 1, 'S', 'AAA', '2021A')
      make_error_items_for_1comm_1hearing_1hi_1vs(report, 1, 'S', 'BBB', '2016A')

      make_error_items_for_1comm_1hearing_1hi(report2, 1, 'H', 'report2', '2021A')
      results = querier.ordered_error_items
      expect(results.length).to eq(30)
      expect(results[29]['clics_committee_id']).to eq('S_BBB_2016A')

      expect(results[0]['clics_committee_id']).to eq('H_AAA_2021A')
      expect(results[0]['clics_key']).to eq('committee_name')
      expect(results[0]['clics_value']).to eq('H_AAA_2021A')
      expect(results[1]['clics_committee_id']).to eq('H_AAA_2021A')
      expect(results[1]['clics_key']).to eq('hearing_items count')
      expect(results[1]['clics_value']).to eq('H_AAA_2021A-hearing1')
      expect(results[2]['clics_committee_id']).to eq('H_AAA_2021A')
      expect(results[2]['clics_key']).to eq('bill_summaries.comm_report_url')
      expect(results[2]['clics_value']).to eq('H_AAA_2021A-hearing1-hi1')
      expect(results[3]['clics_committee_id']).to eq('H_AAA_2021A')
      expect(results[3]['clics_key']).to eq('vote summary order: vote_summaries.vote_time')
      expect(results[3]['clics_value']).to eq('["02/25/2021 08:44 AM", "02/25/2021 09:45 AM", "02/25/2021 10:58 AM"]')
      expect(results[4]['clics_committee_id']).to eq('H_AAA_2021A')
      expect(results[4]['clics_key']).to eq('vote_summaries.vote_motion')
      expect(results[4]['clics_value']).to eq('Adopt amendment')

      expect(results[5]['clics_committee_id']).to eq('H_BBB_2021A')
      expect(results[5]['clics_key']).to eq('committee_name')
      expect(results[5]['clics_value']).to eq('H_BBB_2021A')
      expect(results[6]['clics_committee_id']).to eq('H_BBB_2021A')
      expect(results[6]['clics_key']).to eq('hearing_items count')
      expect(results[6]['clics_value']).to eq('H_BBB_2021A-hearing1')
      expect(results[7]['clics_committee_id']).to eq('H_BBB_2021A')
      expect(results[7]['clics_key']).to eq('bill_summaries.comm_report_url')
      expect(results[7]['clics_value']).to eq('H_BBB_2021A-hearing1-hi1')
      expect(results[8]['clics_committee_id']).to eq('H_BBB_2021A')
      expect(results[8]['clics_key']).to eq('vote summary order: vote_summaries.vote_time')
      expect(results[8]['clics_value']).to eq('["02/25/2021 08:44 AM", "02/25/2021 09:45 AM", "02/25/2021 10:58 AM"]')
      expect(results[9]['clics_committee_id']).to eq('H_BBB_2021A')
      expect(results[9]['clics_key']).to eq('vote_summaries.vote_motion')
      expect(results[9]['clics_value']).to eq('Adopt amendment')

      expect(results[10]['clics_committee_id']).to eq('S_AAA_2021A')
      expect(results[10]['clics_key']).to eq('committee_name')
      expect(results[10]['clics_value']).to eq('S_AAA_2021A')
      expect(results[11]['clics_committee_id']).to eq('S_AAA_2021A')
      expect(results[11]['clics_key']).to eq('hearing_items count')
      expect(results[11]['clics_value']).to eq('S_AAA_2021A-hearing1')
      expect(results[12]['clics_committee_id']).to eq('S_AAA_2021A')
      expect(results[12]['clics_key']).to eq('bill_summaries.comm_report_url')
      expect(results[12]['clics_value']).to eq('S_AAA_2021A-hearing1-hi1')
      expect(results[13]['clics_committee_id']).to eq('S_AAA_2021A')
      expect(results[13]['clics_key']).to eq('vote summary order: vote_summaries.vote_time')
      expect(results[13]['clics_value']).to eq('["02/25/2021 08:44 AM", "02/25/2021 09:45 AM", "02/25/2021 10:58 AM"]')
      expect(results[14]['clics_committee_id']).to eq('S_AAA_2021A')
      expect(results[14]['clics_key']).to eq('vote_summaries.vote_motion')
      expect(results[14]['clics_value']).to eq('Adopt amendment')

      expect(results[15]['clics_committee_id']).to eq('H_AAA_2016B')
      expect(results[15]['clics_key']).to eq('committee_name')
      expect(results[15]['clics_value']).to eq('H_AAA_2016B')
      expect(results[16]['clics_committee_id']).to eq('H_AAA_2016B')
      expect(results[16]['clics_key']).to eq('hearing_items count')
      expect(results[16]['clics_value']).to eq('H_AAA_2016B-hearing1')
      expect(results[17]['clics_committee_id']).to eq('H_AAA_2016B')
      expect(results[17]['clics_key']).to eq('bill_summaries.comm_report_url')
      expect(results[17]['clics_value']).to eq('H_AAA_2016B-hearing1-hi1')
      expect(results[18]['clics_committee_id']).to eq('H_AAA_2016B')
      expect(results[18]['clics_key']).to eq('vote summary order: vote_summaries.vote_time')
      expect(results[18]['clics_value']).to eq('["02/25/2021 08:44 AM", "02/25/2021 09:45 AM", "02/25/2021 10:58 AM"]')
      expect(results[19]['clics_committee_id']).to eq('H_AAA_2016B')
      expect(results[19]['clics_key']).to eq('vote_summaries.vote_motion')
      expect(results[19]['clics_value']).to eq('Adopt amendment')

      expect(results[20]['clics_committee_id']).to eq('H_BBB_2016B')
      expect(results[20]['clics_key']).to eq('committee_name')
      expect(results[20]['clics_value']).to eq('H_BBB_2016B')
      expect(results[21]['clics_committee_id']).to eq('H_BBB_2016B')
      expect(results[21]['clics_key']).to eq('hearing_items count')
      expect(results[21]['clics_value']).to eq('H_BBB_2016B-hearing1')
      expect(results[22]['clics_committee_id']).to eq('H_BBB_2016B')
      expect(results[22]['clics_key']).to eq('bill_summaries.comm_report_url')
      expect(results[22]['clics_value']).to eq('H_BBB_2016B-hearing1-hi1')
      expect(results[23]['clics_committee_id']).to eq('H_BBB_2016B')
      expect(results[23]['clics_key']).to eq('vote summary order: vote_summaries.vote_time')
      expect(results[23]['clics_value']).to eq('["02/25/2021 08:44 AM", "02/25/2021 09:45 AM", "02/25/2021 10:58 AM"]')
      expect(results[24]['clics_committee_id']).to eq('H_BBB_2016B')
      expect(results[24]['clics_key']).to eq('vote_summaries.vote_motion')
      expect(results[24]['clics_value']).to eq('Adopt amendment')

      expect(results[25]['clics_committee_id']).to eq('S_BBB_2016A')
      expect(results[25]['clics_key']).to eq('committee_name')
      expect(results[25]['clics_value']).to eq('S_BBB_2016A')
      expect(results[26]['clics_committee_id']).to eq('S_BBB_2016A')
      expect(results[26]['clics_key']).to eq('hearing_items count')
      expect(results[26]['clics_value']).to eq('S_BBB_2016A-hearing1')
      expect(results[27]['clics_committee_id']).to eq('S_BBB_2016A')
      expect(results[27]['clics_key']).to eq('bill_summaries.comm_report_url')
      expect(results[27]['clics_value']).to eq('S_BBB_2016A-hearing1-hi1')
      expect(results[28]['clics_committee_id']).to eq('S_BBB_2016A')
      expect(results[28]['clics_key']).to eq('vote summary order: vote_summaries.vote_time')
      expect(results[28]['clics_value']).to eq('["02/25/2021 08:44 AM", "02/25/2021 09:45 AM", "02/25/2021 10:58 AM"]')
      expect(results[29]['clics_committee_id']).to eq('S_BBB_2016A')
      expect(results[29]['clics_key']).to eq('vote_summaries.vote_motion')
      expect(results[29]['clics_value']).to eq('Adopt amendment')
    end
  end
end
