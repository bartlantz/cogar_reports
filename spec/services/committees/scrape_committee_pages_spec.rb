# frozen_string_literal: true

require 'rails_helper'
require './spec/services/committees/nokogiri_committee_page_data'

RSpec.describe Committees::ScrapeCommitteePages do
  describe 'Methods' do
    let!(:report) { Reports::CommitteeReport.create! }
    let!(:sns) { Committees::ScrapeCommitteePages.new(report) }
    let!(:nd) { NokogiriCommitteePageData.new }
    let!(:hearing) { Hearing.create!(clics_m_id_digest: 'abc123', meeting_uid: '123abc') }

    it 'make_url(url_filepath)' do
      result = sns.make_url('my_url_filepath')
      expect(result).to eq('http://cwr-staging.denvertech.org/my_url_filepath')
    end
  end
end
