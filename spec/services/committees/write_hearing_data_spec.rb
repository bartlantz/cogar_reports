# frozen_string_literal: true

require 'rails_helper'
require './spec/services/committees/committee_data'

RSpec.describe Committees::WriteHearingData do
  describe 'Methods' do
    let!(:report) { Reports::CommitteeReport.create! }
    let!(:cm) { CommitteeData.new }
    let!(:committee) { Committee.create!(clics_lm_digest: 'comm123', clics_committee_id: 'COMM_123_2016A') }

    let!(:writer_struct) do
      OpenStruct.new(report: report,
                     committee: committee,
                     h_data: nil)
    end
    let!(:whd) { Committees::WriteHearingData.new(writer_struct) }

    it 'performs call with new data' do
      expect(ErrorItem.all.length).to eq(0)
      expect(Hearing.all.length).to eq(0)
      expect(HearingItem.all.length).to eq(0)

      h_data = cm.clics_committee_mtg_data_triple_hi
      writer_struct.h_data = h_data
      whd = Committees::WriteHearingData.new(writer_struct)
      whd.call

      expect(ErrorItem.all.length).to eq(0)
      expect(Hearing.all.length).to eq(1)
      expect(HearingItem.all.length).to eq(3)
    end

    it 'performs call with existing data' do
      h_data = cm.clics_committee_mtg_data_triple_hi
      Hearing.create!(clics_m_id_digest: Digest::SHA256.hexdigest(h_data.to_s), meeting_uid: '123abc')

      expect(ErrorItem.all.length).to eq(0)
      expect(Hearing.all.length).to eq(1)

      writer_struct.h_data = h_data
      whd = Committees::WriteHearingData.new(writer_struct)
      whd.call
      expect(ErrorItem.all.length).to eq(0)
      expect(Hearing.all.length).to eq(1)
    end

    it 'performs get_hearing_items' do
      h_data = cm.clics_committee_mtg_data_triple_hi
      writer_struct.h_data = h_data
      whd = Committees::WriteHearingData.new(writer_struct)
      result = whd.get_hearing_items
      expect(result.length).to eq(3)
      expect(result[0]).to eq('0E0D0D64DAF593A68725867900716A28')
      expect(result[1]).to eq('A88B48BA58FBDC578725867900751199')
      expect(result[2]).to eq('94F3A87005F0A7AE8725867900777819')
    end

    it 'performs get_hearing_items with bad data' do
      h_data = JSON.parse('{"meeting_date":"04/13/2016",
      "meeting_time":"01:42:14 PM",
      "committee_id":["CC_2016A"],
      "meeting_uid":"CC_2016A_20160413_134214",
      "bill_summaries":[{}]
      }')
      writer_struct.h_data = h_data
      whd = Committees::WriteHearingData.new(writer_struct)
      result = whd.get_hearing_items
      expect(result.length).to eq(0)

      h_data = JSON.parse('{"meeting_date":"04/13/2016",
      "meeting_time":"01:42:14 PM",
      "committee_id":["CC_2016A"],
      "meeting_uid":"CC_2016A_20160413_134214"
      }')
      writer_struct.h_data = h_data
      whd = Committees::WriteHearingData.new(writer_struct)
      result = whd.get_hearing_items
      expect(result.length).to eq(0)
    end

    it 'performs create_or_update_hearing(digest, hi)' do
      hi = %w[0E0D0D64DAF593A68725867900716A28 A88B48BA58FBDC578725867900751199 94F3A87005F0A7AE8725867900777819]
      h_data = cm.clics_committee_mtg_data_triple_hi
      writer_struct.h_data = h_data
      whd = Committees::WriteHearingData.new(writer_struct)
      result = whd.create_or_update_hearing('pretend digest', hi)
      expect(result.id).to eq(Hearing.last.id)
      expect(EarlyError.all.length).to eq(0)
    end

    it 'performs create_or_update_hearing(digest, hi) with bad data' do
      hi = %w[0E0D0D64DAF593A68725867900716A28 A88B48BA58FBDC578725867900751199 94F3A87005F0A7AE8725867900777819]
      h_data = cm.clics_committee_mtg_data_single_hi
      h_data['meeting_uid'] = ''
      writer_struct.h_data = h_data
      whd = Committees::WriteHearingData.new(writer_struct)
      result = whd.create_or_update_hearing('pretend digest', hi)
      expect(result).to eq(nil)
      expect(EarlyError.all.length).to eq(1)
    end
  end
end
