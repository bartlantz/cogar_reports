# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Committees::GetClicsCommitteeData do
  describe 'Methods' do
    let!(:report) { Reports::CommitteeReport.create! }
    let!(:gccd) { Committees::GetClicsCommitteeData.new(report) }

    it 'performs make_endpoint()' do
      result = gccd.make_endpoint
      endpoint = 'http://www2.leg.state.co.us/Public/cgaAPI.nsf/api.xsp/committees/summary/lm/2015-01-01T00:00:00Z'
      expect(result).to eq(endpoint)
    end

    it 'performs handle_json_data(endpoint)' do
      expect(EarlyError.all.length).to eq(0)

      result = OpenStruct.new(data: ['Good Stuff!'], error: nil)
      gccd_result = gccd.handle_json_data(gccd.make_endpoint, result)
      expect(EarlyError.all.length).to eq(0)
      expect(gccd_result).to eq(result.data)

      result = OpenStruct.new(data: nil, error: 'pretend error')
      gccd_result = gccd.handle_json_data(gccd.make_endpoint, result)
      expect(gccd_result).to eq(nil)
      expect(EarlyError.all.length).to eq(1)
      expect(EarlyError.first.description).to eq('Could not parse JSON from CLICS committees endpoint: http://www2.leg.state.co.us/Public/cgaAPI.nsf/api.xsp/committees/summary/lm/2015-01-01T00:00:00Z. | Error: pretend error')
    end
  end
end
