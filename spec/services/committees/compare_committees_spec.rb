# frozen_string_literal: true

require 'rails_helper'
require './spec/services/committees/nokogiri_committee_page_data'

RSpec.describe Committees::Comparisons::CompareCommittees do
  describe 'Methods' do
    let!(:report) { ::Reports::CommitteeReport.create! }
    let!(:committee) { Committee.create!(clics_committee_id: 'C_C_ID', clics_lm_digest: '123abc') }
    let!(:tp) do
      TargetPage.create!(parentable_id: Committee.first.id, parentable_type: 'Committee', url_filepath: 'url_filepath',
                         target_slug: 'target_slug')
    end
    let!(:nd) { NokogiriCommitteePageData.new }
    let!(:comparables) do
      OpenStruct.new(report: report,
                     tp: tp,
                     doc: nd.html_page,
                     hearing: nil,
                     hearing_item: nil)
    end
    let!(:cc) { Committees::Comparisons::CompareCommittees.new(comparables) }
    let!(:ei_struct) do
      OpenStruct
        .new(report: comparables.report,
             tp: comparables.tp,
             clics_key: '',
             clics_value: '',
             target_css: '',
             target_value: '',
             sort_error: 0)
    end

    it 'get_ordered_sort_error(target_value, temp_clics_dates)' do
      # matches
      result = cc.get_ordered_sort_error(%w[2021-05-07 2021-02-19], ['2021-05-07T13:37:42Z', '2021-02-19T13:35:55Z'])
      expect(result).to eq(0)

      # both empty
      result = cc.get_ordered_sort_error([], ['0'])
      expect(result).to eq(5)

      # one empty
      result = cc.get_ordered_sort_error(['2021-02-19'], ['0'])
      expect(result).to eq(6)
      result = cc.get_ordered_sort_error([], ['2021-02-19T13:35:55Z'])
      expect(result).to eq(7)

      # mismatched order
      result = cc.get_ordered_sort_error(%w[2021-02-19 2021-05-07], ['2021-05-07T13:37:42Z', '2021-02-19T13:35:55Z'])
      expect(result).to eq(9)
      # missing a value to match
      result = cc.get_ordered_sort_error(%w[2021-02-19 2021-05-07], ['2021-05-07T13:37:42Z'])
      expect(result).to eq(9)
      result = cc.get_ordered_sort_error(['2021-02-19'], ['2021-05-07T13:37:42Z', '2021-02-19T13:35:55Z'])
      expect(result).to eq(9)
    end

    it 'get_target_value(doc, target_css) with good data' do
      target_css = '.cwr_committee_meeting_dates'
      result = cc.get_target_value(comparables.doc, target_css)
      expect(result).to eq(%w[2021-05-07 2021-02-19])
    end

    it 'get_target_value(doc, target_css) with no meetings' do
      target_css = '.cwr_committee_meeting_dates'
      result = cc.get_target_value(nd.html_page_no_data, target_css)
      expect(result).to eq([])
    end

    it 'get_total_clics(tp_field)' do
      result = cc.get_total_clics(nil)
      expect(result).to eq(0)
      result = cc.get_total_clics([])
      expect(result).to eq(0)
      result = cc.get_total_clics(['0'])
      expect(result).to eq(0)
      result = cc.get_total_clics(['2021-05-07T13:37:42Z', '2021-02-19T13:35:55Z'])
      expect(result).to eq(2)
    end

    it 'set_up_ei_for_ordered_values(ei_struct, committee_field, target_value) with mis-ordered data' do
      Committee.create!(clics_committee_id: 'C_C_ID2', clics_lm_digest: '234abc',
                        meeting_dates: ['2021-02-19T13:35:55Z', '2021-05-07T13:37:42Z'])
      tp.update!(parentable_id: Committee.last.id)
      comparables.tp = tp
      ei_struct.tp = tp
      cc2 = Committees::Comparisons::CompareCommittees.new(comparables)
      target_value = %w[2021-02-19 2021-05-07]

      result = cc2.set_up_ei_for_ordered_values(ei_struct, 'meeting_dates', target_value)
      expect(result).to eq(ErrorItem.last)
      expect(result.sort_error).to eq('coga incorrect order or missing values')
      expect(result.clics_key).to eq('meeting_dates order')
      expect(result.clics_value).to eq('["2021-05-07T13:37:42Z", "2021-02-19T13:35:55Z"]')
      expect(result.target_css).to eq('.cwr_committee_meeting_dates')
      expect(result.target_value).to eq('["2021-02-19", "2021-05-07"]')
    end

    it 'set_up_ei_for_ordered_values(ei_struct, committee_field, target_value) with good data' do
      Committee.create!(clics_committee_id: 'C_C_ID2', clics_lm_digest: '234abc',
                        meeting_dates: ['2021-02-19T13:35:55Z', '2021-05-07T13:37:42Z'])
      tp.update!(parentable_id: Committee.last.id)
      comparables.tp = tp
      ei_struct.tp = tp
      cc2 = Committees::Comparisons::CompareCommittees.new(comparables)
      target_value = %w[2021-05-07 2021-02-19]

      result = cc2.set_up_ei_for_ordered_values(ei_struct, 'meeting_dates', target_value)
      expect(result).to eq(ErrorItem.last)
      expect(result.sort_error).to eq('ok')
      expect(result.clics_key).to eq('meeting_dates order')
      expect(result.clics_value).to eq('["2021-05-07T13:37:42Z", "2021-02-19T13:35:55Z"]')
      expect(result.target_css).to eq('.cwr_committee_meeting_dates')
      expect(result.target_value).to eq('["2021-05-07", "2021-02-19"]')
    end

    it 'set_up_ei_for_ordered_values(ei_struct, committee_field, target_value) with bad data' do
      target_value = %w[2021-05-07 2021-02-19]
      result = cc.set_up_ei_for_ordered_values(ei_struct, 'meeting_dates', target_value)
      expect(result).to eq(ErrorItem.last)
      expect(result.sort_error).to eq('clics empty value')
      expect(result.clics_key).to eq('meeting_dates order')
      expect(result.clics_value).to eq('[]')
      expect(result.target_css).to eq('.cwr_committee_meeting_dates')
      expect(result.target_value).to eq('["2021-05-07", "2021-02-19"]')
    end

    it 'set_up_ei_for_count(ei_struct, committee_field, target_value)with good data' do
      Committee.create!(clics_committee_id: 'C_C_ID2', clics_lm_digest: '234abc',
                        meeting_dates: ['2021-02-19T13:35:55Z', '2021-05-07T13:37:42Z'])
      tp.update!(parentable_id: Committee.last.id)
      comparables.tp = tp
      ei_struct.tp = tp
      cc2 = Committees::Comparisons::CompareCommittees.new(comparables)
      target_value = %w[2021-05-07 2021-02-19]

      result = cc2.set_up_ei_for_count(ei_struct, 'meeting_dates', target_value)
      expect(result).to eq(ErrorItem.last)
      expect(result.sort_error).to eq('ok')
      expect(result.clics_key).to eq('meeting_dates count')
      expect(result.clics_value).to eq('2')
      expect(result.target_css).to eq('.cwr_committee_meeting_dates')
      expect(result.target_value).to eq('2')
    end

    it 'set_up_ei_for_count(ei_struct, committee_field, target_value) with bad data' do
      target_value = %w[2021-05-07 2021-02-19]
      result = cc.set_up_ei_for_count(ei_struct, 'meeting_dates', target_value)
      expect(result).to eq(ErrorItem.last)
      expect(result.sort_error).to eq('coga mismatched value')
      expect(result.clics_key).to eq('meeting_dates count')
      expect(result.clics_value).to eq('0')
      expect(result.target_css).to eq('.cwr_committee_meeting_dates')
      expect(result.target_value).to eq('2')
    end

    it 'compare_meeting_dates(comparables, ei_struct)' do
      # This method should make 2 ErrorItems and 2 CommitteeErrorItems, BUT because the initialize method calls the whole set of methods, this test has to pick out just the most recent EI and CEI.

      cc.compare_meeting_dates(comparables, ei_struct)

      expect(ErrorItem.all.length).to eq(2)
      expect(CommitteeErrorItem.all.length).to eq(2)

      last_2_ei = ErrorItem.last(2)
      last_2_cei = CommitteeErrorItem.last(2)
      expect(last_2_cei.first.error_item_id).to eq(last_2_ei.first.id)
      expect(last_2_cei.second.error_item_id).to eq(last_2_ei.second.id)
    end

    it 'compare_clics_comm_id(comparables, ei_struct)' do
      # This method should make 1 ErrorItems and 1 CommitteeErrorItems, BUT because the initialize method calls the whole set of methods, this test has to pick out just the most recent EI and CEI.

      cc.compare_clics_comm_id(comparables, ei_struct)

      expect(ErrorItem.all.length).to eq(1)
      expect(CommitteeErrorItem.all.length).to eq(1)

      expect(CommitteeErrorItem.last.error_item_id).to eq(ErrorItem.last.id)
    end
  end
end
