# frozen_string_literal: true

require 'rails_helper'
require './spec/services/committees/committee_data'

RSpec.describe Committees::GetClicsHearingData do
  describe 'Methods' do
    let!(:report) { Reports::CommitteeReport.create! }
    let!(:cm) { CommitteeData.new }
    let!(:committee) { Committee.create!(clics_lm_digest: 'comm123', clics_committee_id: 'COMM_123_2016A') }
    let!(:gchd) { Committees::GetClicsHearingData.new(report) }

    it 'call with bad data' do
      committee.update(meeting_uids: %w[0 0])

      expect(Hearing.all.length).to eq(0)
    end

    it 'performs make_endpoint(meeting_uid)' do
      committee.update(meeting_uids: %w[0 123abc])
      result = gchd.make_endpoint('A_MTG_123ABC')
      expect(result).to eq('http://www2.leg.state.co.us/Public/cgaAPI.nsf/api.xsp/committees/summary/mId/A_MTG_123ABC')
    end

    it 'handle_json_data(committee, meeting_uid, endpoint, result) with good data' do
      committee.update(meeting_uids: %w[0 123abc])
      Hearing.create!(clics_m_id_digest: 'abc123', meeting_uid: '123abc')
      endpoint = 'http://www2.leg.state.co.us/Public/cgaAPI.nsf/api.xsp/committees/summary/mId/A_MTG_123ABC'
      data = cm.clics_committee_mtg_data_triple_hi
      json_result = OpenStruct.new(data: data, error: nil)

      result = gchd.handle_json_data(committee, '123abc', endpoint, json_result)

      expect(result).to eq(data)
    end

    it 'handle_json_data(committee, meeting_uid, endpoint, result) with errors' do
      committee.update(meeting_uids: %w[0 123abc])
      Hearing.create!(clics_m_id_digest: 'abc123', meeting_uid: '123abc')
      endpoint = 'http://www2.leg.state.co.us/Public/cgaAPI.nsf/api.xsp/committees/summary/mId/A_MTG_123ABC'
      json_result = OpenStruct.new(data: nil, error: 'pretend error')

      result = gchd.handle_json_data(committee, '123abc', endpoint, json_result)

      expect(result).to eq(nil)
      expect(EarlyError.all.length).to eq(1)
      expect(EarlyError.all.first.description).to eq('Could not parse JSON from CLICS committees endpoint: http://www2.leg.state.co.us/Public/cgaAPI.nsf/api.xsp/committees/summary/mId/A_MTG_123ABC. | Error: pretend error')
    end

    it 'handle_json_data(committee, meeting_uid, endpoint, result) with unknown errors' do
      committee.update(meeting_uids: %w[0 123abc])
      Hearing.create!(clics_m_id_digest: 'abc123', meeting_uid: '123abc')
      endpoint = 'http://www2.leg.state.co.us/Public/cgaAPI.nsf/api.xsp/committees/summary/mId/A_MTG_123ABC'
      json_result = OpenStruct.new(data: nil, error: nil)

      result = gchd.handle_json_data(committee, '123abc', endpoint, json_result)

      expect(result).to eq(nil)
      expect(EarlyError.all.length).to eq(1)
      expect(EarlyError.all.first.description).to eq("Could not parse JSON from CLICS committees endpoint: http://www2.leg.state.co.us/Public/cgaAPI.nsf/api.xsp/committees/summary/mId/A_MTG_123ABC. | Error: | Details: unknown error for Committee COMM_123_2016A's meeting_uid: 123abc. All meeting_uids: [\"0\", \"123abc\"].")
    end

    it 'handle_json_data(committee, meeting_uid, endpoint, result) with nil result' do
      committee.update(meeting_uids: %w[0 123abc])
      Hearing.create!(clics_m_id_digest: 'abc123', meeting_uid: '123abc')
      endpoint = 'http://www2.leg.state.co.us/Public/cgaAPI.nsf/api.xsp/committees/summary/mId/A_MTG_123ABC'
      json_result = nil

      result = gchd.handle_json_data(committee, '123abc', endpoint, json_result)

      expect(result).to eq(nil)
      expect(EarlyError.all.length).to eq(1)
      expect(EarlyError.all.first.description).to eq("Could not parse JSON from CLICS committees endpoint: http://www2.leg.state.co.us/Public/cgaAPI.nsf/api.xsp/committees/summary/mId/A_MTG_123ABC. | Error: | Details: unknown error for Committee COMM_123_2016A's meeting_uid: 123abc. All meeting_uids: [\"0\", \"123abc\"].")
    end
  end
end
