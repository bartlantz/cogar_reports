# frozen_string_literal: true

require 'rails_helper'
require './spec/services/committees/committee_data'

RSpec.describe Committees::WriteVoteSummaryData do
  describe 'Methods' do
    let!(:report) { Reports::CommitteeReport.create! }
    let!(:cm) { CommitteeData.new }
    let!(:committee) { Committee.create!(clics_lm_digest: 'comm123', clics_committee_id: 'COMM_123_2016A') }
    let!(:hearing) { Hearing.create!(clics_m_id_digest: 'abc123', meeting_uid: '123abc') }
    let!(:vote_summary) do
      JSON.parse('{"url":"vote_summaries_url123", "vote_time": "string listing time and date from clics", "vote_motion": "PASSED or something"}')
    end
    let!(:writer_struct) do
      OpenStruct.new(report: report,
                     committee: committee,
                     hearing: hearing,
                     hearing_item: hearing_item,
                     h_data: cm.clics_committee_mtg_data_triple_hi,
                     vote_summary: vote_summary)
    end
    let!(:hearing_item) { HearingItem.create!(hearing_id: hearing.id, hearing_item_uuid: 'hearing_item_uuid123') }

    it 'performs call' do
      expect(VoteSummary.all.length).to eq(0)
      expect(EarlyError.all.length).to eq(0)
      Committees::WriteVoteSummaryData.new(writer_struct).call

      expect(VoteSummary.all.length).to eq(1)
      expect(EarlyError.all.length).to eq(0)
      expect(VoteSummary.first.hearing_item_id).to eq(hearing_item.id)
      expect(VoteSummary.first.url).to eq('vote_summaries_url123')
      expect(VoteSummary.first.vote_time).to eq('string listing time and date from clics')
      expect(VoteSummary.first.vote_motion).to eq('PASSED or something')
    end

    it 'update_vote(vote_summary_obj)' do
      digest = Digest::SHA256.hexdigest(vote_summary.to_s)
      vs_obj = VoteSummary.create!(hearing_item_id: hearing_item.id, clics_vote_digest: digest)
      expect(VoteSummary.all.length).to eq(1)
      expect(EarlyError.all.length).to eq(0)

      wvsd = Committees::WriteVoteSummaryData.new(writer_struct)
      wvsd.update_vote(vs_obj)
      expect(VoteSummary.all.length).to eq(1)
      expect(EarlyError.all.length).to eq(0)
      expect(VoteSummary.first.url).to eq('vote_summaries_url123')
      expect(VoteSummary.first.vote_time).to eq('string listing time and date from clics')
      expect(VoteSummary.first.vote_motion).to eq('PASSED or something')
    end

    it 'create_vote(digest) with good data' do
      expect(VoteSummary.all.length).to eq(0)
      expect(EarlyError.all.length).to eq(0)

      wvsd = Committees::WriteVoteSummaryData.new(writer_struct)
      wvsd.create_vote('pretend-digest-1234')
      expect(VoteSummary.all.length).to eq(1)
      expect(EarlyError.all.length).to eq(0)
      expect(VoteSummary.first.hearing_item_id).to eq(hearing_item.id)
      expect(VoteSummary.first.url).to eq('vote_summaries_url123')
      expect(VoteSummary.first.vote_time).to eq('string listing time and date from clics')
      expect(VoteSummary.first.vote_motion).to eq('PASSED or something')
    end

    it 'create_vote(digest) with good data' do
      HearingItem.all.delete_all
      expect(VoteSummary.all.length).to eq(0)
      expect(EarlyError.all.length).to eq(0)

      wvsd = Committees::WriteVoteSummaryData.new(writer_struct)
      wvsd.create_vote('pretend-digest-1234')
      expect(VoteSummary.all.length).to eq(0)
      expect(EarlyError.all.length).to eq(1)
      expect(EarlyError.first.description).to include("Failed to create/update VoteSummary where hearing_item_uuid: hearing_item_uuid123 and bill_number is #{hearing_item.bill_num}.")
      expect(EarlyError.first.description).to include("Additional Details: Committee #{committee.clics_committee_id}, Hearing id: #{hearing.id}.  CLICS data for this object: #{writer_struct.vote_summary}")
    end
  end
end
