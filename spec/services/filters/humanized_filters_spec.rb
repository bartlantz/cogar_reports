# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Filters::HumanizedFilters do
  describe 'Class Methods' do
    let!(:hf) { Filters::HumanizedFilters }
    let!(:orig_params) do
      { early_sort_errors: ['', ''], comparison_sort_errors: ['', ''], sessions: ['', ''], committee_origins: ['', ''],
        committee_ids: ['', ''] }
    end
    let!(:committee1) do
      Committee.create!(clics_lm_digest: 'digest for H_COMM_1_2021A', clics_committee_id: 'H_COMM_1_2021A')
    end
    let!(:committee2) do
      Committee.create!(clics_lm_digest: 'digest for CC_COMM_1_2016A', clics_committee_id: 'CC_COMM_1_2016A')
    end

    it 'self.get_param_val(params, key) for early_sort_errors' do
      params = {}.merge(orig_params)

      params[:early_sort_errors] = ['', 'nil']
      result = hf.get_param_val(params, :early_sort_errors)
      expect(result).to eq(['All'])

      params[:early_sort_errors] = ['', 'none']
      result = hf.get_param_val(params, :early_sort_errors)
      expect(result).to eq(['None'])

      params[:early_sort_errors] = ['', '1']
      result = hf.get_param_val(params, :early_sort_errors)
      expect(result).to eq(['system: error saving object'])

      params[:early_sort_errors] = ['', '2', '1']
      result = hf.get_param_val(params, :early_sort_errors)
      expect(result).to eq(['clics: json error', 'system: error saving object'])
    end

    it 'self.get_param_val(params, key) for comparison_sort_errors' do
      params = {}.merge(orig_params)

      params[:comparison_sort_errors] = ['', 'nil']
      result = hf.get_param_val(params, :comparison_sort_errors)
      expect(result).to eq(['All'])

      params[:comparison_sort_errors] = ['', 'none']
      result = hf.get_param_val(params, :comparison_sort_errors)
      expect(result).to eq(['None'])

      params[:comparison_sort_errors] = ['', '4']
      result = hf.get_param_val(params, :comparison_sort_errors)
      expect(result).to eq(['clics api: json empty'])

      params[:comparison_sort_errors] = ['', '6', '8', '10']
      result = hf.get_param_val(params, :comparison_sort_errors)
      expect(result).to eq(['clics empty value', 'coga mismatched value', 'coga page: application error'])
    end

    it 'self.get_param_val(params, key) for sessions' do
      params = {}.merge(orig_params)

      params[:sessions] = ['', 'nil']
      result = hf.get_param_val(params, :sessions)
      expect(result).to eq(['All'])

      params[:sessions] = ['', 'none']
      result = hf.get_param_val(params, :sessions)
      expect(result).to eq(['None'])

      params[:sessions] = ['', '2020a']
      result = hf.get_param_val(params, :sessions)
      expect(result).to eq(['2020a'])

      params[:sessions] = ['', '2020a', '2017a', '2016a']
      result = hf.get_param_val(params, :sessions)
      expect(result).to eq(%w[2020a 2017a 2016a])
    end

    it 'self.get_param_val(params, key) for committee_origins' do
      params = {}.merge(orig_params)

      params[:committee_origins] = ['', 'nil']
      result = hf.get_param_val(params, :committee_origins)
      expect(result).to eq(['All'])

      params[:committee_origins] = ['', 'none']
      result = hf.get_param_val(params, :committee_origins)
      expect(result).to eq(['None'])

      params[:committee_origins] = ['', 'house']
      result = hf.get_param_val(params, :committee_origins)
      expect(result).to eq(['House'])

      params[:committee_origins] = ['', 'senate', 'joint']
      result = hf.get_param_val(params, :committee_origins)
      expect(result).to eq(%w[Senate Joint])
    end

    it 'self.get_param_val(params, key) for committee_ids' do
      params = {}.merge(orig_params)

      params[:committee_ids] = ['', 'nil']
      result = hf.get_param_val(params, :committee_ids)
      expect(result).to eq(['All'])

      params[:committee_ids] = ['', 'none']
      result = hf.get_param_val(params, :committee_ids)
      expect(result).to eq(['None'])

      params[:committee_ids] = ['', committee1.id.to_s]
      result = hf.get_param_val(params, :committee_ids)
      expect(result).to eq(['H_COMM_1_2021A'])

      params[:committee_ids] = ['', committee1.id.to_s, committee2.id.to_s]
      result = hf.get_param_val(params, :committee_ids)
      expect(result).to eq(%w[H_COMM_1_2021A CC_COMM_1_2016A])
    end

    it 'day_diff_msg(params)' do
      params = {}.merge(orig_params)
      params[:day_diff] = '1'

      result = hf.day_diff_msg(params)
      expect(result).to eq('New issues since Yesterday')

      # check that it shouldn't matter if there are other params
      params[:early_sort_errors] = ['', '1', '2']
      result = hf.day_diff_msg(params)
      expect(result).to eq('New issues since Yesterday')

      params[:day_diff] = '3'
      result = hf.day_diff_msg(params)
      expect(result).to eq('New issues since 3 days ago')

      params[:day_diff] = '5'
      result = hf.day_diff_msg(params)
      expect(result).to eq('New issues since 5 days ago')

      params[:day_diff] = '7'
      result = hf.day_diff_msg(params)
      expect(result).to eq('New issues since 1 week ago')
    end

    it 'self.humanized_filters(params)' do
      params = {}.merge(orig_params)

      result = hf.humanized_filters(params)
      expect(result).to eq('All Report Errors')

      params[:early_sort_errors] = ['', '1', '2']
      params[:comparison_sort_errors] = ['', '1', '2']
      params[:sessions] = ['', '2021a', '2020a']
      params[:committee_origins] = ['', 'senate', 'joint']
      params[:committee_ids] = ['', committee1.id.to_s, committee2.id.to_s]
      result = hf.humanized_filters(params)
      expect(result).to eq(' Early Errors: system: error saving object, clics: json error | Comparison Errors: system: error saving object, clics: json error | Sessions: 2021a, 2020a | Comm Origins: Senate, Joint | Committee Ids: H_COMM_1_2021A, CC_COMM_1_2016A ')

      params = {}.merge(orig_params)
      params[:day_diff] = '1'
      result = hf.humanized_filters(params)
      expect(result).to eq('New issues since Yesterday')
    end
  end
end
