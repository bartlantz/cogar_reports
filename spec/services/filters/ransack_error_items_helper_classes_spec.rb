# frozen_string_literal: true

require 'rails_helper'

RSpec.describe do
  describe 'Suporting Ransack Classes' do
    let!(:report) { Reports::CommitteeReport.create! }
    let!(:orig_params) do
      { early_sort_errors: ['', ''],
        comparison_sort_errors: ['', ''],
        sessions: ['', ''],
        committee_origins: ['', ''],
        committee_ids: ['', ''],
        report_id: report.id.to_s }
    end
    let!(:q_all) do
      {
        q: {},
        committees: {},
        hearings: {},
        hearing_items: {},
        vote_summaries: {}
      }
    end

    # Committee 1, all errors are 0
    let!(:c1) do
      Committee.create!(clics_lm_digest: 'digest for H_COMM_1_2021A', clics_committee_id: 'H_COMM_1_2021A',
                        session_year: '2021A')
    end
    let!(:t1) do
      TargetPage.create!(parentable_id: c1.id,
                         parentable_type: 'Committee',
                         target_slug: 'my_committee_1',
                         url_filepath: '/committees/house/2021a/my_committee_1')
    end
    let!(:c1_h1) { Hearing.create!(clics_m_id_digest: 'digest for H_1_2021A_1234', meeting_uid: 'H_1_2021A_1234') }
    let!(:ch1) { CommitteeHearing.create!(committee_id: c1.id, hearing_id: c1_h1.id) }
    let!(:c1_h1_hi1) { HearingItem.create!(hearing_id: c1_h1.id, hearing_item_uuid: 'H_1_2021A_1234_AAA') }
    let!(:ee_c1) do
      ErrorItem.create!(reportable_id: report.id,
                        reportable_type: report.type,
                        target_page_id: t1.id,
                        sort_error: 0)
    end
    let!(:ee_c1_h1) do
      ErrorItem.create!(reportable_id: report.id,
                        reportable_type: report.type,
                        target_page_id: t1.id,
                        sort_error: 0)
    end
    let!(:ee_c1_h1_hi1) do
      ErrorItem.create!(reportable_id: report.id,
                        reportable_type: report.type,
                        target_page_id: t1.id,
                        sort_error: 0)
    end
    let!(:cee_c1) { CommitteeErrorItem.create!(error_item_id: ee_c1.id, committee_id: c1.id) }
    let!(:hee_c1_h1) { HearingErrorItem.create!(error_item_id: ee_c1_h1.id, hearing_id: c1_h1.id) }
    let!(:hiee_c1_h1_hi1) do
      HearingItemErrorItem.create!(error_item_id: ee_c1_h1_hi1.id, hearing_item_id: c1_h1_hi1.id)
    end

    # Committee 2, all errors are 5
    let!(:c2) do
      Committee.create!(clics_lm_digest: 'digest for J_COMM_2_2022A', clics_committee_id: 'J_COMM_2_2022A',
                        session_year: '2022A')
    end
    let!(:t2) do
      TargetPage.create!(parentable_id: c2.id,
                         parentable_type: 'Committee',
                         target_slug: 'my_committee_1',
                         url_filepath: '/committees/house/2022a/my_committee_1')
    end
    let!(:c2_h1) { Hearing.create!(clics_m_id_digest: 'digest for H_2_2022A_1234', meeting_uid: 'H_2_2022A_1234') }
    let!(:ch2) { CommitteeHearing.create!(committee_id: c2.id, hearing_id: c2_h1.id) }
    let!(:c2_h1_hi1) { HearingItem.create!(hearing_id: c2_h1.id, hearing_item_uuid: 'H_2_2022A_1234_AAA') }
    let!(:ee_c2) do
      ErrorItem.create!(reportable_id: report.id,
                        reportable_type: report.type,
                        target_page_id: t2.id,
                        sort_error: 5)
    end
    let!(:ee_c2_h1) do
      ErrorItem.create!(reportable_id: report.id,
                        reportable_type: report.type,
                        target_page_id: t2.id,
                        sort_error: 5)
    end
    let!(:ee_c2_h1_hi1) do
      ErrorItem.create!(reportable_id: report.id,
                        reportable_type: report.type,
                        target_page_id: t2.id,
                        sort_error: 5)
    end
    let!(:cee_c2) { CommitteeErrorItem.create!(error_item_id: ee_c2.id, committee_id: c2.id) }
    let!(:hee_c2_h1) { HearingErrorItem.create!(error_item_id: ee_c2_h1.id, hearing_id: c2_h1.id) }
    let!(:hiee_c2) { HearingItemErrorItem.create!(error_item_id: ee_c2_h1_hi1.id, hearing_item_id: c2_h1_hi1.id) }

    # Committee 3, all errors are 8
    let!(:c3) do
      Committee.create!(clics_lm_digest: 'digest for CC_COMM_3_2023A', clics_committee_id: 'CC_COMM_3_2023A',
                        session_year: '2023A')
    end
    let!(:t3) do
      TargetPage.create!(parentable_id: c3.id,
                         parentable_type: 'Committee',
                         target_slug: 'my_committee_1',
                         url_filepath: '/committees/house/2023a/my_committee_1')
    end
    let!(:c3_h1) { Hearing.create!(clics_m_id_digest: 'digest for H_3_2023A_1234', meeting_uid: 'H_3_2023A_1234') }
    let!(:ch3) { CommitteeHearing.create!(committee_id: c3.id, hearing_id: c3_h1.id) }
    let!(:c3_h1_hi1) { HearingItem.create!(hearing_id: c3_h1.id, hearing_item_uuid: 'H_3_2023A_1234_AAA') }
    let!(:ee_c3) do
      ErrorItem.create!(reportable_id: report.id,
                        reportable_type: report.type,
                        target_page_id: t3.id,
                        sort_error: 8)
    end
    let!(:ee_c3_h1) do
      ErrorItem.create!(reportable_id: report.id,
                        reportable_type: report.type,
                        target_page_id: t3.id,
                        sort_error: 8)
    end
    let!(:ee_c3_h1_hi1) do
      ErrorItem.create!(reportable_id: report.id,
                        reportable_type: report.type,
                        target_page_id: t3.id,
                        sort_error: 8)
    end
    let!(:cee_c3) { CommitteeErrorItem.create!(error_item_id: ee_c3.id, committee_id: c3.id) }
    let!(:hee_c3_h1) { HearingErrorItem.create!(error_item_id: ee_c3_h1.id, hearing_id: c3_h1.id) }
    let!(:hiee_c3) { HearingItemErrorItem.create!(error_item_id: ee_c3_h1_hi1.id, hearing_item_id: c3_h1_hi1.id) }

    let!(:c3_h1_hi1_vs1) do
      VoteSummary.create!(hearing_item_id: c3_h1_hi1.id, clics_vote_digest: 'some-digest-for-vs1')
    end
    let!(:t_for_vs1) do
      TargetPage.create!(parentable_id: c3_h1_hi1_vs1.id,
                         parentable_type: 'VoteSummary',
                         url_filepath: 'path-at-cogar-site')
    end
    let!(:ee_c3_h1_hi1_vs1) do
      ErrorItem.create!(reportable_id: report.id,
                        reportable_type: report.type,
                        target_page_id: t_for_vs1.id,
                        sort_error: 8)
    end
    let!(:vsee_c3) do
      VoteSummaryErrorItem.create!(error_item_id: ee_c3_h1_hi1_vs1.id, vote_summary_id: c3_h1_hi1_vs1.id)
    end

    it 'RansackSortErrors' do
      params = {}.merge(orig_params)

      params[:comparison_sort_errors] = ['', '']
      sacked = Filters::RansackSortErrors.new(q_all, params).call
      expect(sacked).to eq(q_all)

      params[:comparison_sort_errors] = ['', '0']
      sacked = Filters::RansackSortErrors.new(q_all, params).call
      expect(sacked[:q][:sort_error_in]).to eq([0])

      params[:comparison_sort_errors] = ['', '0', '5']
      sacked = Filters::RansackSortErrors.new(q_all, params).call
      expect(sacked[:q][:sort_error_in]).to eq([0, 5])

      params[:comparison_sort_errors] = ['', '0', '5', '8', '10']
      sacked = Filters::RansackSortErrors.new(q_all, params).call
      expect(sacked[:q][:sort_error_in]).to eq([0, 5, 8, 10])

      params[:comparison_sort_errors] = ['', '10']
      sacked = Filters::RansackSortErrors.new(q_all, params).call
      expect(sacked[:q][:sort_error_in]).to eq([10])
    end

    it 'RansackCommitteeIds' do
      params = {}.merge(orig_params)
      c_key = :committee_error_item_committee_id_in
      h_key = :hearing_error_item_hearing_committee_hearings_committee_id_in
      hi_key = :hearing_item_error_item_hearing_item_hearing_committee_hearings_committee_id_in
      vs_key = :vote_summary_error_item_vote_summary_hearing_item_hearing_committee_hearings_committee_id_in

      params[:committee_ids] = ['', c1.id.to_s]
      sacked = Filters::RansackCommitteeIds.new(q_all, params).call
      expect(sacked[:committees]).to eq({ c_key => [c1.id] })
      expect(sacked[:hearings]).to eq({ h_key => [c1.id] })
      expect(sacked[:hearing_items]).to eq({ hi_key => [c1.id] })
      expect(sacked[:vote_summaries]).to eq({ vs_key => [c1.id] })

      params[:committee_ids] = ['', c1.id.to_s, c2.id.to_s]
      sacked = Filters::RansackCommitteeIds.new(q_all, params).call
      expect(sacked[:committees]).to eq({ c_key => [c1.id, c2.id] })
      expect(sacked[:hearings]).to eq({ h_key => [c1.id, c2.id] })
      expect(sacked[:hearing_items]).to eq({ hi_key => [c1.id, c2.id] })
      expect(sacked[:vote_summaries]).to eq({ vs_key => [c1.id, c2.id] })

      params[:committee_ids] = ['', c1.id.to_s, '999']
      sacked = Filters::RansackCommitteeIds.new(q_all, params).call
      expect(sacked[:committees]).to eq({ c_key => [c1.id, 999] })
      expect(sacked[:hearings]).to eq({ h_key => [c1.id, 999] })
      expect(sacked[:hearing_items]).to eq({ hi_key => [c1.id, 999] })
      expect(sacked[:vote_summaries]).to eq({ vs_key => [c1.id, 999] })
    end

    it 'RansackSessions' do
      params = {}.merge(orig_params)
      c_key = :committee_error_item_committee_session_year_i_cont_any
      h_key = :hearing_error_item_hearing_committee_hearings_committee_session_year_i_cont_any
      hi_key = :hearing_item_error_item_hearing_item_hearing_committee_hearings_committee_session_year_i_cont_any
      vs_key = :vote_summary_error_item_vote_summary_hearing_item_hearing_committee_hearings_i_cont_any

      params[:sessions] = ['', c1.session_year]
      sacked = Filters::RansackSessions.new(q_all, params).call
      expect(sacked[:committees]).to eq({ c_key => [c1.session_year] })
      expect(sacked[:hearings]).to eq({ h_key => [c1.session_year] })
      expect(sacked[:hearing_items]).to eq({ hi_key => [c1.session_year] })
      expect(sacked[:hearing_items]).to eq({ hi_key => [c1.session_year] })

      params[:sessions] = ['', c1.session_year, c3.session_year]
      sacked = Filters::RansackSessions.new(q_all, params).call
      expect(sacked[:committees]).to eq({ c_key => [c1.session_year, c3.session_year] })
      expect(sacked[:hearings]).to eq({ h_key => [c1.session_year, c3.session_year] })
      expect(sacked[:hearing_items]).to eq({ hi_key => [c1.session_year, c3.session_year] })
      expect(sacked[:hearing_items]).to eq({ hi_key => [c1.session_year, c3.session_year] })

      params[:sessions] = ['', 'Fake']
      sacked = Filters::RansackSessions.new(q_all, params).call
      expect(sacked[:committees]).to eq({ c_key => ['Fake'] })
      expect(sacked[:hearings]).to eq({ h_key => ['Fake'] })
      expect(sacked[:hearing_items]).to eq({ hi_key => ['Fake'] })
      expect(sacked[:hearing_items]).to eq({ hi_key => ['Fake'] })
    end

    it 'RansackCommitteeOrigins' do
      params = {}.merge(orig_params)
      c_key = :committee_error_item_committee_clics_committee_id_start_any
      h_key = :hearing_error_item_hearing_committee_hearings_committee_clics_committee_id_start_any
      hi_key = :hearing_item_error_item_hearing_item_hearing_committee_hearings_committee_clics_committee_id_start_any
      vs_key = :vote_summary_error_item_vote_summary_hearing_item_hearing_committee_hearings_committee_clics_committee_id_start_any

      params[:committee_origins] = ['', 'house']
      sacked = Filters::RansackCommitteeOrigins.new(q_all, params).call
      expect(sacked[:committees]).to eq({ c_key => ['H_'] })
      expect(sacked[:hearings]).to eq({ h_key => ['H_'] })
      expect(sacked[:hearing_items]).to eq({ hi_key => ['H_'] })
      expect(sacked[:vote_summaries]).to eq({ vs_key => ['H_'] })

      params[:committee_origins] = ['', 'house', 'conference']
      sacked = Filters::RansackCommitteeOrigins.new(q_all, params).call
      expect(sacked[:committees]).to eq({ c_key => %w[H_ CC_] })
      expect(sacked[:hearings]).to eq({ h_key => %w[H_ CC_] })
      expect(sacked[:hearing_items]).to eq({ hi_key => %w[H_ CC_] })
      expect(sacked[:vote_summaries]).to eq({ vs_key => %w[H_ CC_] })

      params[:committee_origins] = ['', 'joint']
      sacked = Filters::RansackCommitteeOrigins.new(q_all, params).call
      expect(sacked[:committees]).to eq({ c_key => %w[I_ J_ O_ SE_] })
      expect(sacked[:hearings]).to eq({ h_key => %w[I_ J_ O_ SE_] })
      expect(sacked[:hearing_items]).to eq({ hi_key => %w[I_ J_ O_ SE_] })
      expect(sacked[:vote_summaries]).to eq({ vs_key => %w[I_ J_ O_ SE_] })
    end
  end
end
