# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Filters::RansackErrorItems do
  describe 'Methods' do
    let!(:report) { Reports::CommitteeReport.create! }
    let!(:orig_params) do
      { early_sort_errors: ['', ''], comparison_sort_errors: ['', ''], sessions: ['', ''], committee_origins: ['', ''],
        committee_ids: ['', ''], report_id: report.id.to_s }
    end

    # Committee 1, all errors are 0
    let!(:c1) do
      Committee.create!(clics_lm_digest: 'digest for H_COMM_1_2021A', clics_committee_id: 'H_COMM_1_2021A',
                        session_year: '2021A')
    end
    let!(:t1) do
      TargetPage.create!(parentable_id: c1.id,
                         parentable_type: 'Committee',
                         target_slug: 'my_committee_1',
                         url_filepath: '/committees/house/2021a/my_committee_1')
    end
    let!(:c1_h1) { Hearing.create!(clics_m_id_digest: 'digest for H_1_2021A_1234', meeting_uid: 'H_1_2021A_1234') }
    let!(:ch1) { CommitteeHearing.create!(committee_id: c1.id, hearing_id: c1_h1.id) }
    let!(:c1_h1_hi1) { HearingItem.create!(hearing_id: c1_h1.id, hearing_item_uuid: 'H_1_2021A_1234_AAA') }
    let!(:ee_c1) do
      ErrorItem.create!(reportable_id: report.id,
                        reportable_type: report.type,
                        target_page_id: t1.id,
                        sort_error: 0)
    end
    let!(:ee_c1_h1) do
      ErrorItem.create!(reportable_id: report.id,
                        reportable_type: report.type,
                        target_page_id: t1.id,
                        sort_error: 0)
    end
    let!(:ee_c1_h1_hi1) do
      ErrorItem.create!(reportable_id: report.id,
                        reportable_type: report.type,
                        target_page_id: t1.id,
                        sort_error: 0)
    end
    let!(:cee_c1) { CommitteeErrorItem.create!(error_item_id: ee_c1.id, committee_id: c1.id) }
    let!(:hee_c1_h1) { HearingErrorItem.create!(error_item_id: ee_c1_h1.id, hearing_id: c1_h1.id) }
    let!(:hiee_c1_h1_hi1) do
      HearingItemErrorItem.create!(error_item_id: ee_c1_h1_hi1.id, hearing_item_id: c1_h1_hi1.id)
    end

    # Committee 2, all errors are 5
    let!(:c2) do
      Committee.create!(clics_lm_digest: 'digest for J_COMM_2_2022A', clics_committee_id: 'J_COMM_2_2022A',
                        session_year: '2022A')
    end
    let!(:t2) do
      TargetPage.create!(parentable_id: c2.id,
                         parentable_type: 'Committee',
                         target_slug: 'my_committee_1',
                         url_filepath: '/committees/house/2022a/my_committee_1')
    end
    let!(:c2_h1) { Hearing.create!(clics_m_id_digest: 'digest for H_2_2022A_1234', meeting_uid: 'H_2_2022A_1234') }
    let!(:ch2) { CommitteeHearing.create!(committee_id: c2.id, hearing_id: c2_h1.id) }
    let!(:c2_h1_hi1) { HearingItem.create!(hearing_id: c2_h1.id, hearing_item_uuid: 'H_2_2022A_1234_AAA') }
    let!(:ee_c2) do
      ErrorItem.create!(reportable_id: report.id,
                        reportable_type: report.type,
                        target_page_id: t2.id,
                        sort_error: 5)
    end
    let!(:ee_c2_h1) do
      ErrorItem.create!(reportable_id: report.id,
                        reportable_type: report.type,
                        target_page_id: t2.id,
                        sort_error: 5)
    end
    let!(:ee_c2_h1_hi1) do
      ErrorItem.create!(reportable_id: report.id,
                        reportable_type: report.type,
                        target_page_id: t2.id,
                        sort_error: 5)
    end
    let!(:cee_c2) { CommitteeErrorItem.create!(error_item_id: ee_c2.id, committee_id: c2.id) }
    let!(:hee_c2_h1) { HearingErrorItem.create!(error_item_id: ee_c2_h1.id, hearing_id: c2_h1.id) }
    let!(:hiee_c2) { HearingItemErrorItem.create!(error_item_id: ee_c2_h1_hi1.id, hearing_item_id: c2_h1_hi1.id) }

    # Committee 3, all errors are 8
    let!(:c3) do
      Committee.create!(clics_lm_digest: 'digest for CC_COMM_3_2023A', clics_committee_id: 'CC_COMM_3_2023A',
                        session_year: '2023A')
    end
    let!(:t3) do
      TargetPage.create!(parentable_id: c3.id,
                         parentable_type: 'Committee',
                         target_slug: 'my_committee_1',
                         url_filepath: '/committees/house/2023a/my_committee_1')
    end
    let!(:c3_h1) { Hearing.create!(clics_m_id_digest: 'digest for H_3_2023A_1234', meeting_uid: 'H_3_2023A_1234') }
    let!(:ch3) { CommitteeHearing.create!(committee_id: c3.id, hearing_id: c3_h1.id) }
    let!(:c3_h1_hi1) { HearingItem.create!(hearing_id: c3_h1.id, hearing_item_uuid: 'H_3_2023A_1234_AAA') }
    let!(:ee_c3) do
      ErrorItem.create!(reportable_id: report.id,
                        reportable_type: report.type,
                        target_page_id: t3.id,
                        sort_error: 8)
    end
    let!(:ee_c3_h1) do
      ErrorItem.create!(reportable_id: report.id,
                        reportable_type: report.type,
                        target_page_id: t3.id,
                        sort_error: 8)
    end
    let!(:ee_c3_h1_hi1) do
      ErrorItem.create!(reportable_id: report.id,
                        reportable_type: report.type,
                        target_page_id: t3.id,
                        sort_error: 8)
    end
    let!(:cee_c3) { CommitteeErrorItem.create!(error_item_id: ee_c3.id, committee_id: c3.id) }
    let!(:hee_c3_h1) { HearingErrorItem.create!(error_item_id: ee_c3_h1.id, hearing_id: c3_h1.id) }
    let!(:hiee_c3) { HearingItemErrorItem.create!(error_item_id: ee_c3_h1_hi1.id, hearing_item_id: c3_h1_hi1.id) }

    let!(:c3_h1_hi1_vs1) do
      VoteSummary.create!(hearing_item_id: c3_h1_hi1.id, clics_vote_digest: 'some-digest-for-vs1')
    end
    let!(:t_for_vs1) do
      TargetPage.create!(parentable_id: c3_h1_hi1_vs1.id,
                         parentable_type: 'VoteSummary',
                         url_filepath: 'path-at-cogar-site')
    end
    let!(:ee_c3_h1_hi1_vs1) do
      ErrorItem.create!(reportable_id: report.id,
                        reportable_type: report.type,
                        target_page_id: t_for_vs1.id,
                        sort_error: 8)
    end
    let!(:vsee_c3) do
      VoteSummaryErrorItem.create!(error_item_id: ee_c3_h1_hi1_vs1.id, vote_summary_id: c3_h1_hi1_vs1.id)
    end

    it 'search using sort_errors' do
      params = {}.merge(orig_params)

      params[:comparison_sort_errors] = ['', '']
      rei = Filters::RansackErrorItems.new(params, report.id)
      result = rei.call
      expect(result.length).to eq(10)

      params[:comparison_sort_errors] = ['', '0']
      rei = Filters::RansackErrorItems.new(params, report.id)
      result = rei.call
      expect(result.length).to eq(3)

      params[:comparison_sort_errors] = ['', '0', '5']
      rei = Filters::RansackErrorItems.new(params, report.id)
      result = rei.call
      expect(result.length).to eq(6)

      params[:comparison_sort_errors] = ['', '0', '5', '8', '10']
      rei = Filters::RansackErrorItems.new(params, report.id)
      result = rei.call
      expect(result.length).to eq(10)

      params[:comparison_sort_errors] = ['', '10']
      rei = Filters::RansackErrorItems.new(params, report.id)
      result = rei.call
      expect(result.length).to eq(0)
    end

    it 'search using committee_ids' do
      params = {}.merge(orig_params)

      params[:committee_ids] = ['', '']
      rei = Filters::RansackErrorItems.new(params, report.id)
      result = rei.call
      expect(result.length).to eq(10)

      params[:committee_ids] = ['', 'nil']
      rei = Filters::RansackErrorItems.new(params, report.id)
      result = rei.call
      expect(result.length).to eq(10)

      params[:committee_ids] = ['', c1.id.to_s]
      rei = Filters::RansackErrorItems.new(params, report.id)
      result = rei.call
      expect(result.length).to eq(3)

      params[:committee_ids] = ['', c1.id.to_s, c2.id.to_s]
      rei = Filters::RansackErrorItems.new(params, report.id)
      result = rei.call
      expect(result.length).to eq(6)

      params[:committee_ids] = ['', c1.id.to_s, '999']
      rei = Filters::RansackErrorItems.new(params, report.id)
      result = rei.call
      expect(result.length).to eq(3)
    end

    it 'search using sessions' do
      params = {}.merge(orig_params)

      params[:sessions] = ['', '']
      rei = Filters::RansackErrorItems.new(params, report.id)
      result = rei.call
      expect(result.length).to eq(10)

      params[:sessions] = ['', 'nil']
      rei = Filters::RansackErrorItems.new(params, report.id)
      result = rei.call
      expect(result.length).to eq(10)

      params[:sessions] = ['', c1.session_year]
      rei = Filters::RansackErrorItems.new(params, report.id)
      result = rei.call
      expect(result.length).to eq(3)

      params[:sessions] = ['', c1.session_year, c3.session_year]
      rei = Filters::RansackErrorItems.new(params, report.id)
      result = rei.call
      expect(result.length).to eq(7)

      params[:sessions] = ['', 'Fake']
      rei = Filters::RansackErrorItems.new(params, report.id)
      result = rei.call
      expect(result.length).to eq(0)
    end

    it 'search using committee_origins' do
      params = {}.merge(orig_params)

      params[:committee_origins] = ['', '']
      rei = Filters::RansackErrorItems.new(params, report.id)
      result = rei.call
      expect(result.length).to eq(10)

      params[:committee_origins] = ['', 'nil']
      rei = Filters::RansackErrorItems.new(params, report.id)
      result = rei.call
      expect(result.length).to eq(10)

      params[:committee_origins] = ['', 'house']
      rei = Filters::RansackErrorItems.new(params, report.id)
      result = rei.call
      expect(result.length).to eq(3)

      params[:committee_origins] = ['', 'house', 'conference']
      rei = Filters::RansackErrorItems.new(params, report.id)
      result = rei.call
      expect(result.length).to eq(7)

      params[:committee_origins] = ['', 'joint']
      rei = Filters::RansackErrorItems.new(params, report.id)
      result = rei.call
      expect(result.length).to eq(3)
    end

    it 'returns the right results for .call()' do
      params = {}.merge(orig_params)
      params[:comparison_sort_errors] = ['', 'none']
      params[:committee_ids] = ['', c1.id.to_s]
      params[:sessions] = ['', c1.session_year]
      params[:committee_origins] = ['', '']
      result = Filters::RansackErrorItems.new(params, report.id).call
      expect(result.length).to eq(0)

      params = {}.merge(orig_params)
      params[:comparison_sort_errors] = ['', '0']
      params[:committee_ids] = ['', c1.id.to_s]
      params[:sessions] = ['', c1.session_year]
      params[:committee_origins] = ['', '']
      result = Filters::RansackErrorItems.new(params, report.id).call
      expect(result.length).to eq(3)

      params = {}.merge(orig_params)
      params[:comparison_sort_errors] = ['', '5']
      params[:committee_ids] = ['', c1.id.to_s]
      params[:sessions] = ['', c1.session_year]
      params[:committee_origins] = ['', '']
      result = Filters::RansackErrorItems.new(params, report.id).call
      expect(result.length).to eq(0)

      params = {}.merge(orig_params)
      params[:comparison_sort_errors] = ['', '0']
      params[:committee_ids] = ['', c1.id.to_s]
      params[:sessions] = ['', 'nil']
      params[:committee_origins] = ['', '']
      result = Filters::RansackErrorItems.new(params, report.id).call
      expect(result.length).to eq(3)

      params = {}.merge(orig_params)
      params[:comparison_sort_errors] = ['', '0']
      params[:committee_ids] = ['', c1.id.to_s]
      params[:sessions] = ['', 'nil']
      params[:committee_origins] = ['', 'joint']
      result = Filters::RansackErrorItems.new(params, report.id).call
      expect(result.length).to eq(3)

      params = {}.merge(orig_params)
      params[:comparison_sort_errors] = ['', '']
      params[:committee_ids] = ['', 'nil']
      params[:sessions] = ['', '']
      params[:committee_origins] = ['', 'conference']
      result = Filters::RansackErrorItems.new(params, report.id).call
      expect(result.length).to eq(4)

      params = {}.merge(orig_params)
      params[:comparison_sort_errors] = ['', '5']
      params[:committee_ids] = ['', c1.id.to_s]
      params[:sessions] = ['', c2.session_year]
      params[:committee_origins] = ['', '']
      result = Filters::RansackErrorItems.new(params, report.id).call
      expect(result.length).to eq(0)

      params = {}.merge(orig_params)
      params[:comparison_sort_errors] = ['', '5']
      params[:committee_ids] = ['', '']
      params[:sessions] = ['', c2.session_year]
      params[:committee_origins] = ['', 'house']
      result = Filters::RansackErrorItems.new(params, report.id).call
      expect(result.length).to eq(0)

      params = {}.merge(orig_params)
      params[:comparison_sort_errors] = ['', '8']
      params[:committee_ids] = ['', '']
      params[:sessions] = ['', '']
      params[:committee_origins] = ['', '']
      result = Filters::RansackErrorItems.new(params, report.id).call
      expect(result.length).to eq(4)
    end
  end
end
