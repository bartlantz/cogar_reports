# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Filters::DayDiffErrors do
  describe 'Class Methods' do
    let!(:dde) { Filters::DayDiffErrors }

    #  I need a set of reports from different day, or just be able to change the created_at to suite my needs. Reports need to have at least 1 EE and EI, and some decoys to make sure that not all are collected.

    let!(:report1) { Reports::CommitteeReport.create! }
    let!(:report2) { Reports::CommitteeReport.create! }
    let!(:report_decoy) { Reports::CommitteeReport.create! }

    let!(:c1) do
      Committee.create!(clics_lm_digest: 'digest for H_APP_2021A', clics_committee_id: 'H_APP_2021A')
    end
    let!(:t1) do
      TargetPage.create!(parentable_id: c1.id,
                         parentable_type: 'Committee',
                         target_slug: 'Appropriations',
                         url_filepath: '/approps/and/stuff')
    end
    let!(:ee1_r1) do
      EarlyError.create!(reportable_id: report1.id,
                         reportable_type: report1.type,
                         description: 'something bad',
                         err_digest: 'shared_early_error_1_for_report_1',
                         sort_error: 8)
    end
    let!(:ei1_r1) do
      ErrorItem.create!(reportable_id: report1.id,
                        reportable_type: report1.type,
                        target_page_id: t1.id,
                        err_digest: 'shared_error_item_1_for_report_1',
                        sort_error: 8)
    end
    let!(:ee2_r1) do
      EarlyError.create!(reportable_id: report1.id,
                         reportable_type: report1.type,
                         description: 'something bad',
                         err_digest: 'unique_early_error_2_for_report_1',
                         sort_error: 8)
    end
    let!(:ei2_r1) do
      ErrorItem.create!(reportable_id: report1.id,
                        reportable_type: report1.type,
                        target_page_id: t1.id,
                        err_digest: 'unique_error_item_2_for_report_1',
                        sort_error: 8)
    end
    let!(:ee1_r2) do
      EarlyError.create!(reportable_id: report2.id,
                         reportable_type: report2.type,
                         description: 'something bad',
                         err_digest: 'shared_early_error_1_for_report_1',
                         sort_error: 8)
    end
    let!(:ei1_r2) do
      ErrorItem.create!(reportable_id: report2.id,
                        reportable_type: report2.type,
                        target_page_id: t1.id,
                        err_digest: 'shared_error_item_1_for_report_1',
                        sort_error: 8)
    end
    let!(:ee1_r_decoy) do
      EarlyError.create!(reportable_id: report_decoy.id,
                         reportable_type: report_decoy.type,
                         description: 'something bad',
                         err_digest: 'early_error_1_for_report_decoy',
                         sort_error: 8)
    end
    let!(:ei1_r_decoy) do
      ErrorItem.create!(reportable_id: report_decoy.id,
                        reportable_type: report_decoy.type,
                        target_page_id: t1.id,
                        err_digest: 'error_item_1_for_report_decoy',
                        sort_error: 8)
    end

    context 'Issues since yesterday' do
      it 'returns zero EE and EI when there are no matching results' do
        report2.update(created_at: report1.created_at - 2.days)
        report_decoy.update(created_at: report1.created_at - 2.days)

        result = dde.new('1', report1, 'early_errors')
        expect(result.errs.length).to eq(0)

        result = dde.new('1', report1, 'error_items')
        expect(result.errs.length).to eq(0)
      end

      it 'returns the right number of EE and EI' do
        report2.update(created_at: report1.created_at - 1.days)
        report_decoy.update(created_at: report1.created_at - 2.days)

        result = dde.new('1', report1, 'early_errors')
        expect(result.errs.length).to eq(1)
        expect(result.errs[0]).to eq(ee2_r1)

        result = dde.new('1', report1, 'error_items')
        expect(result.errs.length).to eq(1)
        expect(result.errs[0]).to eq(ei2_r1)
      end

      it 'get_difference(report_errs, dd_errs)' do
        report2.update(created_at: report1.created_at - 1.days)
        report_decoy.update(created_at: report1.created_at - 2.days)

        report_errs = ErrorItem.where(reportable_id: report1.id).pluck(:err_digest)
        dd_errs = ErrorItem.where(reportable_id: report2.id).pluck(:err_digest)

        result = dde.new('1', report1, 'error_items').get_difference(report_errs, dd_errs)
        expect(result.length).to eq(1)
        expect(result[0]).to eq(ei2_r1.err_digest)
      end

      it 'get_non_shared_errs(report, diff_date_report, err_kind)' do
        report2.update(created_at: report1.created_at - 1.days)
        report_decoy.update(created_at: report1.created_at - 2.days)

        result = dde.new('1', report1, 'early_errors').get_non_shared_errs(report1, report2, 'early_errors')
        expect(result.length).to eq(1)
        expect(result[0]).to eq(ee2_r1.err_digest)

        result = dde.new('1', report1, 'error_items').get_non_shared_errs(report1, report2, 'error_items')
        expect(result.length).to eq(1)
        expect(result[0]).to eq(ei2_r1.err_digest)
      end
    end

    context 'Issues since 3 days ago' do
      it 'returns the right number of EE and EI' do
        report2.update(created_at: report1.created_at - 3.days)
        report_decoy.update(created_at: report1.created_at - 2.days)

        result = dde.new('3', report1, 'early_errors')
        expect(result.errs.length).to eq(1)
        expect(result.errs[0]).to eq(ee2_r1)

        result = dde.new('3', report1, 'error_items')
        expect(result.errs.length).to eq(1)
        expect(result.errs[0]).to eq(ei2_r1)
      end
    end

    context 'Issues since 5 days ago' do
      it 'returns the right number of EE and EI' do
        report2.update(created_at: report1.created_at - 5.days)
        report_decoy.update(created_at: report1.created_at - 6.days)

        result = dde.new('5', report1, 'early_errors')
        expect(result.errs.length).to eq(1)
        expect(result.errs[0]).to eq(ee2_r1)

        result = dde.new('5', report1, 'error_items')
        expect(result.errs.length).to eq(1)
        expect(result.errs[0]).to eq(ei2_r1)
      end
    end

    context 'Issues since 1 week ago' do
      it 'returns the right number of EE and EI' do
        report2.update(created_at: report1.created_at - 7.days)
        report_decoy.update(created_at: report1.created_at - 6.days)

        result = dde.new('7', report1, 'early_errors')
        expect(result.errs.length).to eq(1)
        expect(result.errs[0]).to eq(ee2_r1)

        result = dde.new('7', report1, 'error_items')
        expect(result.errs.length).to eq(1)
        expect(result.errs[0]).to eq(ei2_r1)
      end
    end
  end
end
