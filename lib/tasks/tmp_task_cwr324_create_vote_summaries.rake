# frozen_string_literal: true

# to run this, call:
# rake tmp_task_cwr324_create_vote_summaries:one_time

namespace :tmp_task_cwr324_create_vote_summaries do
  desc 'Create VoteSummary for each HearingItem vote_summary'
  task one_time: :environment do

    # Because the Jobs are written to only update or access resources if there
    # are changes to the CLICS data, this rake task need to access every hearing
    # and hearing_item. It also needs to hit the CLICS endpoints for each of the
    # meeting ids, which are stored in the Committee objects. Therefore, we need
    # to iterate through committees to get the meeting_uids and make use of the
    # existing endpoint logic, then iterate through Hearings by looking them up
    # with the meeting_uid

    report = Reports::CommitteeReport.create!
    writer_struct = OpenStruct.new(report: report)
    very_end_errors = {}
    #-------------- ITERATE THROUGH COMMITTEES
    Committee.find_each do |c|
      writer_struct.committee = c

      #-------------- ITERATE THROUGH HEARINGS VIA MEETING-UID
      c.meeting_uids.each do |mtg_uid|
        hearing = Hearing.find_by(meeting_uid: mtg_uid)
        writer_struct.hearing = hearing

        h_data = Committees::GetClicsHearingData.new(report).get_clics_hearing_data(c, mtg_uid)
        if !h_data || h_data.dig('error')
          err = h_data.dig('error') if h_data && h_data.dig('error')
          msg =  "Error getting h_data. Hearing's meeting_uid: #{mtg_uid}, and h_data.error:#{err}"
          very_end_errors[mtg_uid] = msg
        end
        next if !h_data || h_data.dig('error')

        writer_struct.h_data = h_data
        clics_hearing_items =  h_data.dig('bill_summaries')

        #-------------- ITERATE THROUGH CLICSBILLSUMMARIES FOR HEARINGITEMS
        clics_hearing_items.each do |bill_summ|
          hi = HearingItem.find_by(hearing_item_uuid: bill_summ.dig('hearing_item_uuid'))
          writer_struct.hearing_item = hi
          vs = bill_summ.dig('vote_summaries')

          #-------------- ITERATE THROUGH CLICSVOTESUMMARIES
          vs.each do |v|
            writer_struct.vote_summary = v
            Committees::WriteVoteSummaryData.new(writer_struct).call
          end
        end
      end
    end
    p very_end_errors
    puts "Report# #{report.id}"
    puts "Task Complete"
  end
end
