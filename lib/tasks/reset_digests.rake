# frozen_string_literal: true

# to run this, call:
# rake reset_digests:one_time

namespace :reset_digests do
  desc 'Resetting Digests forces the Services to update all resources'
  task one_time: :environment do

    Committee.all.find_each do |obj|
      obj.update!(clics_lm_digest: "reset#{obj.clics_lm_digest}")
    end

    Hearing.all.find_each do |obj|
      obj.update!(clics_m_id_digest: "reset#{obj.clics_m_id_digest}")
    end

    puts "Rake Task Complete"

  end
end
