# frozen_string_literal: true

# to run this, call:
# rake tmp_task_cwr286_ordering_early_errors:one_time

namespace :tmp_task_cwr286_ordering_early_errors do
  desc 'Add data to session_year for EarlyErrors'
  task one_time: :environment do

    Sessions.all.each do |sesh|
      q = {}
      q[:description_i_cont_any] = [sesh]
      ee_by_sesh = EarlyError.ransack(q).result
      ee_by_sesh.update_all(session_year: sesh)
    end

    puts "Task Complete"
  end
end
